/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.constants.NotificationChannels
import io.vertretungsplan.client.android.constants.NotificationIds
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.ui.main.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class NotifyInstitution (private val registry: AppRegistry, private val context: Context) {
    private val lock = Mutex()
    private val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    fun updateNotificationByInstitutionIdAsync(institutionId: String) {
        GlobalScope.launch {
            updateNotificationByInstitutionId(institutionId)
        }
    }

    private suspend fun updateNotificationByInstitutionId(institutionId: String) {
        lock.withLock {
            registry.database.runInTransaction {
                val plan = registry.database.institutionPlanItemDao().getPlanItemsForNotificationSync(institutionId = institutionId).filter { it.hasChanges }
                val message = registry.database.institutionMessageDao().getMessagesForNotificationSync(institutionId = institutionId)
                val file = registry.database.institutionFileDao().getFilesForNotificationSync(institutionId = institutionId)

                val hasItemsForNotification = plan.isNotEmpty() or message.isNotEmpty() or file.isNotEmpty()

                if (hasItemsForNotification) {
                    val title = registry.database.institution().getByIdSync(institutionId)?.title ?: institutionId
                    val messageItems = plan.map { context.getString(R.string.content_list_plan_lesson, it.lesson) } + message.map { it.title } + file.map { it.title }

                    notificationManager.notify(
                        institutionId,
                        NotificationIds.PLAN_UPDATE_NOTIFICATION,
                        NotificationCompat.Builder(context, NotificationChannels.PLAN_CHANGE_NOTIFICATIONS)
                            .setContentTitle(title)
                            .setContentText(messageItems.joinToString(separator = "\n"))
                            .setContentIntent(
                                PendingIntent.getActivity(
                                    context,
                                    0,
                                    Intent(context, MainActivity::class.java)
                                        .putExtra(MainActivity.EXTRA_INSTITUTION_ID, institutionId),
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
                                    else PendingIntent.FLAG_UPDATE_CURRENT
                                )
                            )
                            .setSmallIcon(R.drawable.ic_stat_vp)
                            .setOnlyAlertOnce(true)
                            .build()
                    )
                } else {
                    notificationManager.cancel(institutionId, NotificationIds.PLAN_UPDATE_NOTIFICATION)
                }
            }
        }
    }

    fun dismissNotificationByInstitutionIdAsync(institutionId: String) {
        GlobalScope.launch (Dispatchers.IO) {
            lock.withLock {
                // dismiss at os level
                notificationManager.cancel(institutionId, NotificationIds.PLAN_UPDATE_NOTIFICATION)

                // mark as read at the database
                registry.database.runInTransaction {
                    registry.database.institutionPlanItemDao().setNotificationDismissedByInstitutionIdSync(institutionId = institutionId)
                    registry.database.institutionMessageDao().setNotificationDismissedByInstitutionIdSync(institutionId = institutionId)
                    registry.database.institutionFileDao().setNotificationDismissedByInstitutionIdSync(institutionId = institutionId)
                }
            }
        }
    }
}