/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.AppDatabase
import io.vertretungsplan.client.android.data.cas.CasStorage
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.collect

class ClearStorage(private val casStorage: CasStorage, private val database: AppDatabase) {
    companion object {
        private const val LOG_TAG = "ClearStorage"
    }

    private val shouldDoCleanup = Channel<Boolean>(capacity = Channel.CONFLATED)

    fun requestCleanup() {
        shouldDoCleanup.trySend(true)
    }

    init {
        GlobalScope.launch {
            shouldDoCleanup.consumeEach {
                doCleanupCoroutine()

                // used to debounce
                delay(1000)
            }
        }
    }

    private suspend fun doCleanupCoroutine() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "do cleanup")
        }

        withContext(Dispatchers.IO) {
            casStorage.getAllHashes().collect { hash ->
                val count = database.institutionFileElementDao().countHashCoroutine(hash)

                if (count == 0L) {
                    if (BuildConfig.DEBUG) {
                        Log.d(LOG_TAG, "remove file $hash")
                    }

                    casStorage.getFileByHash(hash).delete()
                }
            }
        }
    }
}