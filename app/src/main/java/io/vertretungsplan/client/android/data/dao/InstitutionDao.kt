/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.vertretungsplan.client.android.data.model.Institution

@Dao
interface InstitutionDao {
    @Query("SELECT * FROM institution")
    fun getListLive(): LiveData<List<Institution>>

    @Query("SELECT * FROM institution WHERE id = :institutionId")
    fun getByIdSync(institutionId: String): Institution?

    @Query("SELECT * FROM institution WHERE id = :institutionId")
    suspend fun getByIdCoroutine(institutionId: String): Institution?

    @Query("SELECT * FROM institution WHERE id = :institutionId")
    fun getByIdLive(institutionId: String): LiveData<Institution?>

    @Insert
    fun insert(item: Institution)

    @Update
    fun update(item: Institution)

    @Query("DELETE FROM institution WHERE id NOT IN (:currentIds)")
    fun deleteOtherIds(currentIds: List<String>)

    @Query("DELETE FROM institution")
    fun deleteAll()
}