/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model.content

import android.util.JsonReader
import io.vertretungsplan.client.android.data.util.parseList

data class FileContentItem(
    val id: String,
    val type: FileContentItemType,
    val title: String,
    val mimeType: String,
    val lastModified: Long?,
    val file: List<FileContentFileElement>,
    val notify: Boolean
) {
    companion object {
        fun parse(reader: JsonReader): FileContentItem {
            var id: String? = null
            var type: FileContentItemType? = null
            var title: String? = null
            var mimeType: String? = null
            var lastModified: Long? = null
            var file: List<FileContentFileElement>? = null
            var notify: Boolean? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "id" -> id = reader.nextString()
                    "type" -> type = when (reader.nextString()) {
                        "plan" -> FileContentItemType.Plan
                        "download" -> FileContentItemType.Download
                        else -> throw IllegalArgumentException()
                    }
                    "title" -> title = reader.nextString()
                    "mimeType" -> mimeType = reader.nextString()
                    "lastModified" -> lastModified = reader.nextLong()
                    "file" -> file = reader.parseList { FileContentFileElement.parse(reader) }
                    "notify" -> notify = reader.nextBoolean()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return FileContentItem(
                id = id!!,
                type = type!!,
                title = title!!,
                mimeType = mimeType!!,
                lastModified = lastModified,
                file = file!!,
                notify = notify!!
            )
        }
    }

    init {
        if (file.isEmpty()) {
            throw IllegalArgumentException()
        }
    }
}

enum class FileContentItemType {
    Plan, Download
}