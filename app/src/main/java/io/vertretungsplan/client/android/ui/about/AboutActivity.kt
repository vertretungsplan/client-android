/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.about

import android.os.Bundle
import android.text.Spanned
import android.text.style.URLSpan
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.LinkAnnotation
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentActivity
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.ui.AppTheme

class AboutActivity : FragmentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableEdgeToEdge()

        setContent {
            AppTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(stringResource(R.string.about_title))
                            }
                        )
                    },
                    content = { insets ->
                        Column(
                            Modifier
                                .verticalScroll(rememberScrollState())
                                .padding(insets)
                                .padding(8.dp),
                            verticalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            TextCard(
                                title = stringResource(R.string.app_name),
                                text = stringResource(R.string.about_version, BuildConfig.VERSION_NAME)
                            )

                            TextCard(
                                title = stringResource(R.string.about_source_title),
                                text = getText(R.string.about_source_text)
                            )

                            TextCard(
                                title = stringResource(R.string.about_license),
                                text = getText(R.string.about_license_text)
                            )

                            TextCard(
                                title = stringResource(R.string.about_libraries_title),
                                text = getText(R.string.about_libraries_text)
                            )
                        }
                    }
                )
            }
        }
    }

    @Composable
    private fun TextCard(title: String, text: CharSequence) {
        TextCard(
            title,
            buildAnnotatedString {
                append(text)

                if (text is Spanned) {
                    text.getSpans(0, length, URLSpan::class.java).forEach { span ->
                        span as URLSpan

                        val start = text.getSpanStart(span)
                        val end = text.getSpanEnd(span)

                        addStyle(
                            SpanStyle(
                                textDecoration = TextDecoration.Underline
                            ),
                            start, end
                        )

                        addLink(
                            LinkAnnotation.Url(span.url),
                            start, end
                        )
                    }
                }
            }
        )
    }

    @Composable
    private fun TextCard(title: String, text: AnnotatedString) {
        Card {
            Column(
                Modifier.fillMaxWidth().padding(8.dp)
            ) {
                Text(
                    title,
                    style = MaterialTheme.typography.headlineMedium
                )

                Text(text)
            }
        }
    }
}
