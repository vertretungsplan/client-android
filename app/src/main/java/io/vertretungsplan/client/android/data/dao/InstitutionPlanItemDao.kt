/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.vertretungsplan.client.android.data.model.InstitutionPlanItem

@Dao
interface InstitutionPlanItemDao {
    @Query("SELECT * FROM institution_plan_item WHERE institution_id = :institutionId ORDER BY sort ASC")
    fun getPlanItemsByInstitutionLive(institutionId: String): LiveData<List<InstitutionPlanItem>>

    @Query("SELECT * FROM institution_plan_item WHERE institution_id = :institutionId AND content_bucket_id = :contentBucketId")
    fun getPlanItemsByInstitutionAndBucketIdSync(institutionId: String, contentBucketId: String): List<InstitutionPlanItem>

    @Query("DELETE FROM institution_plan_item WHERE id IN (:itemIds)")
    fun removePlanItemsByIdsSync(itemIds: List<Int>)

    @Query("DELETE FROM institution_plan_item WHERE institution_id = :institutionId AND content_bucket_id NOT IN (:contentBucketsToKeep)")
    fun removePlanItemsInOtherBuckets(institutionId: String, contentBucketsToKeep: List<String>)

    @Insert
    fun insert(items: List<InstitutionPlanItem>)

    @Update
    fun update(items: List<InstitutionPlanItem>)

    @Query("UPDATE institution_plan_item SET was_notification_dismissed = 1 WHERE institution_id = :institutionId")
    fun setNotificationDismissedByInstitutionIdSync(institutionId: String)

    @Query("SELECT * FROM institution_plan_item WHERE institution_id = :institutionId AND was_notification_dismissed = 0")
    fun getPlanItemsForNotificationSync(institutionId: String): List<InstitutionPlanItem>
}