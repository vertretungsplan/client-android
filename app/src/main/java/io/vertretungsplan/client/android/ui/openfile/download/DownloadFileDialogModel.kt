/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.download

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.vertretungsplan.client.android.data.feature.DownloadFileProgress
import io.vertretungsplan.client.android.data.feature.DownloadFileStatus
import io.vertretungsplan.client.android.registry.AppRegistry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

class DownloadFileDialogModel: ViewModel() {
    private var hadInit = false
    private val statusInternal = MutableLiveData<DownloadFileStatus>()
    val status: LiveData<DownloadFileStatus> = statusInternal

    fun init(registry: AppRegistry, url: String, sha512: String, size: Long) {
        if (hadInit) {
            return
        }

        hadInit = true

        statusInternal.value = DownloadFileProgress(current = 0, max = null)

        GlobalScope.launch (Dispatchers.Main) {
            registry.downloadFile.doDownloadAsync(
                url = url,
                sha512 = sha512,
                size = size
            ).consumeEach { status ->
                statusInternal.value = status
            }
        }
    }
}