/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data

import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.database.MatrixCursor
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.provider.OpenableColumns
import android.text.TextUtils
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.FileNotFoundException
import java.io.IOException

class CasProvider: ContentProvider() {
    companion object {
        private const val AUTHORITY = "${BuildConfig.APPLICATION_ID}.data"
        const val BASE = "content://$AUTHORITY/file"

        const val URL_PARAM_SHA512 = "sha512"
        const val URL_PARAM_TITLE = "title"
        const val URL_PARAM_MIME_TYPE = "mimeType"

        fun createFileUri(sha512: String, title: String, mimeType: String) = Uri.parse(BASE).buildUpon()
            .appendQueryParameter(URL_PARAM_SHA512, sha512)
            .appendQueryParameter(URL_PARAM_TITLE, title)
            .appendQueryParameter(URL_PARAM_MIME_TYPE, mimeType)
            .build()
    }

    val registry: AppRegistry by lazy { DefaultAppRegistry.with(context!!) }

    override fun onCreate() = true

    override fun getType(uri: Uri): String? {
        return uri.getQueryParameter(URL_PARAM_MIME_TYPE)?.toMediaTypeOrNull()?.let {
            "${it.type}/${it.subtype}"
        }
    }

    override fun query(uri: Uri, projection: Array<out String>?, selection: String?, selectionArgs: Array<out String>?, sortOrder: String?): Cursor? {
        val resultProjection = projection ?: arrayOf(OpenableColumns.DISPLAY_NAME, OpenableColumns.SIZE)
        val resultCursor = MatrixCursor(resultProjection)

        val sha512 = uri.getQueryParameter(URL_PARAM_SHA512)!!
        var title = uri.getQueryParameter(URL_PARAM_TITLE)!!
        var mimeType = uri.getQueryParameter(URL_PARAM_MIME_TYPE)!!

        // add file extension (for some bad viewer apps)
        if (!TextUtils.isEmpty(mimeType) && mimeType.contains("/")) {
            mimeType = mimeType.substring(mimeType.lastIndexOf("/") + 1)

            if (mimeType.contains("x-")) {
                mimeType = mimeType.substring(2)
            }

            title += ".$mimeType"
        }

        val builder = resultCursor.newRow()

        for (column in resultProjection) {
            if (OpenableColumns.DISPLAY_NAME == column) {
                builder.add(title)
            } else if (OpenableColumns.SIZE == column) {
                builder.add(registry.cas.getFileByHash(sha512).length())
            } else {
                builder.add(null)
            }
        }


        return resultCursor
    }

    override fun openFile(uri: Uri, mode: String): ParcelFileDescriptor? {
        if (mode != "r") {
            throw IOException("unsupported access mode")
        }

        val file = registry.cas.getFileByHash(uri.getQueryParameter(URL_PARAM_SHA512)!!)

        if (!file.exists()) {
            throw FileNotFoundException()
        }

        return ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY)
    }

    override fun insert(p0: Uri, p1: ContentValues?): Uri? {
        throw NotImplementedError()
    }

    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        throw NotImplementedError()
    }

    override fun delete(p0: Uri, p1: String?, p2: Array<out String>?): Int {
        throw NotImplementedError()
    }
}