/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.TypeConverters
import io.vertretungsplan.client.android.data.util.parseList

@Entity(
    tableName = "institution_config_bucket",
    primaryKeys = [
        "institution_id",
        "config_bucket_id"
    ],
    foreignKeys = [
        ForeignKey(
            entity = Institution::class,
            parentColumns = ["id"],
            childColumns = ["institution_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
@TypeConverters(
    ConfigurationScreenItemListConverter::class,
    ConditionSetItemMapConverter::class,
    ContentBucketSetItemListConverter::class
)
data class InstitutionConfigBucket (
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "config_bucket_id")
    val configBucketId: String,
    val config: List<ConfigurationScreenItem>,
    @ColumnInfo(name = "content_buckets")
    val contentBuckets: List<ContentBucketSetItem>,
    val conditions: Map<String, ConditionSetItem>,
    @ColumnInfo(name = "config_validation_condition_id")
    val configValidationConditionId: String
) {
    companion object {
        fun parse(reader: JsonReader, institutionId: String, configBucketId: String): InstitutionConfigBucket {
            var config: List<ConfigurationScreenItem>? = null
            var contentBuckets: List<ContentBucketSetItem>? = null
            var conditions: Map<String, ConditionSetItem>? = null
            var configValidationConditionId: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "config" -> config = reader.parseList { ConfigurationScreenItem.parse(reader) }
                    "contentBucketSets" -> contentBuckets = reader.parseList { ContentBucketSetItem.parse(reader) }
                    "conditionSets" -> conditions = reader.parseList { ConditionSetItem.parse(reader) }.associateBy { it.id }
                    "configValidationConditionId" -> configValidationConditionId = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return InstitutionConfigBucket(
                institutionId = institutionId,
                configBucketId = configBucketId,
                config = config!!,
                contentBuckets = contentBuckets!!,
                conditions = conditions!!,
                configValidationConditionId = configValidationConditionId!!
            )
        }
    }
}