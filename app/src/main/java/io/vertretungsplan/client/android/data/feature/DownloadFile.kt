/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2021 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.client.http.assertSuccess
import io.vertretungsplan.client.android.registry.AppRegistry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import okhttp3.Request
import okio.buffer
import okio.sink
import java.io.IOException
import java.security.MessageDigest

class DownloadFile(private val appRegistry: AppRegistry) {
    companion object {
        private const val LOG_TAG = "DownloadFile"
    }

    private val casStorage = appRegistry.cas
    private val httpClient = appRegistry.httpClient

    private val downloadJobLock = Mutex()
    private val downloadJobs = mutableMapOf<String, BroadcastChannel<DownloadFileStatus>>()

    fun doDownloadAsync(url: String, sha512: String, size: Long): ReceiveChannel<DownloadFileStatus> {
        val (result, isNew) = runBlocking {
            downloadJobLock.withLock {
                val oldJob = downloadJobs[sha512]

                if (oldJob != null) {
                    oldJob to false
                } else {
                    val newJob = BroadcastChannel<DownloadFileStatus>(Channel.CONFLATED)

                    downloadJobs[sha512] = newJob

                    newJob to true
                }
            }
        }

        if (isNew) {
            GlobalScope.launch(Dispatchers.IO) {
                try {
                    if (casStorage.getFileByHash(sha512).exists()) {
                        appRegistry.backgroundDownload.descheduleDownload(sha512 = sha512)

                        // nothing to do anymore
                        result.send(DownloadFileStatusDone(wasDownloadedAlready = true))
                        return@launch
                    }

                    val tempFile = casStorage.createNewTempFile()

                    try {
                        // download file and calculate checksum
                        val digest = MessageDigest.getInstance("SHA512")
                        var totalBytesRead = 0L

                        httpClient.newCall(
                            Request.Builder()
                                .url(url)
                                .build()
                        ).execute().use { response ->
                            response.assertSuccess()

                            val max = response.body!!.contentLength()

                            if (max != -1L && max != size) {
                                throw IOException("unexpected file size; expected $size but got $max")
                            }

                            tempFile.sink().buffer()
                                .use { sink ->
                                    val buffer = ByteArray(1024 * 128 /* 0,1 MB */)

                                    response.body!!.byteStream().use { inputStream ->
                                        while (true) {
                                            val bytesRead = inputStream.read(buffer)

                                            if (bytesRead == -1) {
                                                break
                                            }

                                            digest.update(buffer, 0, bytesRead)
                                            sink.write(buffer, 0, bytesRead)

                                            totalBytesRead += bytesRead

                                            if (totalBytesRead > size) {
                                                throw IOException("unexpected file size; expected $size but got $totalBytesRead")
                                            }

                                            result.send(DownloadFileProgress(
                                                current = totalBytesRead,
                                                max = size
                                            ))
                                        }
                                    }
                                }
                        }

                        if (totalBytesRead != size) {
                            throw IOException("unexpected file size; expected $size but got $totalBytesRead")
                        }

                        // verify checksum
                        val calculatedSha512 = digest.digest().joinToString (separator = "") { String.format("%02x", it) }

                        if (sha512 != calculatedSha512) {
                            throw IOException("file checksum $calculatedSha512 (${tempFile.length()} did not match the expected $sha512")
                        }

                        // move to target
                        val targetFile = casStorage.getFileByHash(sha512)

                        if (!targetFile.parentFile!!.isDirectory) {
                            if (!targetFile.parentFile!!.mkdirs()) {
                                throw IOException("could not create target directory")
                            }
                        }

                        if (!tempFile.renameTo(targetFile)) {
                            throw IOException("could not move temp file to target")
                        }
                    } finally {
                        tempFile.delete()
                    }

                    appRegistry.backgroundDownload.descheduleDownload(sha512 = sha512)

                    result.send(DownloadFileStatusDone(wasDownloadedAlready = false))
                } catch (ex: Exception) {
                    if (BuildConfig.DEBUG) {
                        Log.w(LOG_TAG, "could not download file", ex)
                    }

                    result.send(DownloadFileFailed(ex))
                } finally {
                    downloadJobLock.withLock {
                        downloadJobs.remove(sha512)
                    }
                }
            }
        }

        return result.openSubscription()
    }
}

sealed class DownloadFileStatus
data class DownloadFileProgress(val current: Long, val max: Long?): DownloadFileStatus()
data class DownloadFileStatusDone(val wasDownloadedAlready: Boolean): DownloadFileStatus()
data class DownloadFileFailed(val error: Exception): DownloadFileStatus()