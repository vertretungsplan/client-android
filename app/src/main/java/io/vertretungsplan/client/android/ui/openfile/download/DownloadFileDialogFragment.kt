/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.download

import android.app.Dialog
import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.feature.DownloadFileFailed
import io.vertretungsplan.client.android.data.feature.DownloadFileProgress
import io.vertretungsplan.client.android.data.feature.DownloadFileStatusDone
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.openfile.OpenFile

class DownloadFileDialogFragment: DialogFragment() {
    companion object {
        private const val ARG_INSTITUTION_ID = "institutionId"
        private const val ARG_CONTENT_BUCKET_ID = "contentBucketId"
        private const val ARG_FILE_ID = "fileId"
        private const val ARG_TITLE = "title"
        private const val ARG_SHA512 = "sha512"
        private const val ARG_MIMETYPE = "mimeType"
        private const val ARG_URL = "url"
        private const val ARG_SIZE = "size"
        private const val DIALOG_TAG = "DownloadFileDialogFragment"

        fun newInstance(
            institutionId: String,
            contentBucketId: String,
            fileId: String,
            title: String,
            sha512: String,
            mimeType: String,
            url: String,
            size: Long
        ) = DownloadFileDialogFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_INSTITUTION_ID, institutionId)
                putString(ARG_CONTENT_BUCKET_ID, contentBucketId)
                putString(ARG_FILE_ID, fileId)
                putString(ARG_TITLE, title)
                putString(ARG_SHA512, sha512)
                putString(ARG_MIMETYPE, mimeType)
                putString(ARG_URL, url)
                putLong(ARG_SIZE, size)
            }
        }
    }

    private val model: DownloadFileDialogModel by lazy { ViewModelProviders.of(this).get(DownloadFileDialogModel::class.java) }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = ProgressDialog(context!!, theme).let { dialog ->
        dialog.setMessage(getString(R.string.dialog_download_file))
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)

        val institutionId = arguments!!.getString(ARG_INSTITUTION_ID)!!
        val contentBucketId = arguments!!.getString(ARG_CONTENT_BUCKET_ID)!!
        val fileId = arguments!!.getString(ARG_FILE_ID)!!
        val title = arguments!!.getString(ARG_TITLE)!!
        val sha512 = arguments!!.getString(ARG_SHA512)!!
        val mimeType = arguments!!.getString(ARG_MIMETYPE)!!
        val url = arguments!!.getString(ARG_URL)!!
        val size = arguments!!.getLong(ARG_SIZE)

        val registry = DefaultAppRegistry.with(context!!)

        model.init(
            url = url,
            sha512 = sha512,
            size = size,
            registry = registry
        )

        model.status.observe(this, Observer { status ->
            when (status) {
                is DownloadFileProgress -> {
                    dialog.isIndeterminate = status.max == null

                    if (status.max != null) {
                        dialog.max = 1000
                        dialog.progress = (status.current * 1000L / status.max).toInt()
                    }
                }
                is DownloadFileFailed -> {
                    Toast.makeText(context!!, R.string.viewer_loading_page_failed, Toast.LENGTH_SHORT).show()

                    dismiss()
                }
                is DownloadFileStatusDone -> {
                    OpenFile.openDownloadedFile(
                        institutionId = institutionId,
                        contentBucketId = contentBucketId,
                        fileId = fileId,
                        context = context!!,
                        fragmentManager = fragmentManager!!,
                        title = title,
                        sha512 = sha512,
                        mimeType = mimeType
                    )

                    dismiss()
                }
            }
        })

        dialog
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}