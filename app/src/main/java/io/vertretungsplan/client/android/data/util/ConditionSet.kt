/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2021 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.util

import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.model.ConditionSetItem

object ConditionSet {
    private const val LOG_TAG = "ConditionSet"

    fun evaluate(
        params: Map<String, String>,
        conditionSet: Map<String, ConditionSetItem>,
        conditionId: String,
        blacklistedParams: Set<String> = emptySet()
    ): Boolean {
        if (conditionId.isEmpty()) {
            throw ConditionSetIdMustNotBeEmptyException()
        }

        if (blacklistedParams.contains(conditionId)) {
            throw ConditionSetEvaluationRecursionException()
        }

        if (conditionId.startsWith("_")) {
            // internal

            return when (conditionId) {
                "_true" -> true
                "_false" -> false
                else -> {
                    if (conditionId.startsWith("_doesClientSupport")) false
                    else throw InvalidInternalConditionException()
                }
            }
        } else {
            // from the set
            val condition = conditionSet[conditionId] ?: throw InvalidConditionSetIdException(conditionId)

            val result = when (condition.type) {
                "and" -> {
                    val newBlacklistedParams = blacklistedParams + setOf(conditionId)

                    evaluate(params, conditionSet, condition.left, newBlacklistedParams) &&
                            evaluate(params, conditionSet, condition.right, newBlacklistedParams)
                }
                "or" -> {
                    val newBlacklistedParams = blacklistedParams + setOf(conditionId)

                    evaluate(params, conditionSet, condition.left, newBlacklistedParams) ||
                            evaluate(params, conditionSet, condition.right, newBlacklistedParams)
                }
                "not" -> {
                    if (condition.right.isNotEmpty()) {
                        throw RightMustBeEmptyException()
                    }

                    !evaluate(params, conditionSet, condition.left, blacklistedParams + setOf(conditionId))
                }
                "paramIs" -> {
                    if (condition.left.isEmpty()) {
                        throw MissingParamIdException()
                    }

                    val paramValue = params[condition.left] ?: ""

                    paramValue == condition.right
                }
                "paramNotEmpty" -> {
                    if (condition.left.isEmpty()) {
                        throw MissingParamIdException()
                    }

                    if (condition.right.isNotEmpty()) {
                        throw RightMustBeEmptyException()
                    }

                    val paramValue = params[condition.left] ?: ""

                    paramValue.isNotEmpty()
                }
                else -> throw UnknownConditionSetTypeException()
            }

            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "evaluate($condition) = $result")
            }

            return result
        }
    }
}

sealed class ConditionSetEvaluationException: RuntimeException()
class InvalidInternalConditionException(): ConditionSetEvaluationException()
class ConditionSetEvaluationRecursionException: ConditionSetEvaluationException()
data class InvalidConditionSetIdException(val conditionSetId: String): ConditionSetEvaluationException()
class UnknownConditionSetTypeException(): ConditionSetEvaluationException()
class RightMustBeEmptyException(): ConditionSetEvaluationException()
class ConditionSetIdMustNotBeEmptyException(): ConditionSetEvaluationException()
class MissingParamIdException(): ConditionSetEvaluationException()