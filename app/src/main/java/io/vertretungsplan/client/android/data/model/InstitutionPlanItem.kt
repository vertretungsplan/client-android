/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "institution_plan_item",
    indices = [
        Index(
            value = [
                "institution_id",
                "content_bucket_id"
            ]
        )
    ]
)
data class InstitutionPlanItem(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "content_bucket_id")
    val contentBucketId: String,
    val date: String,
    @ColumnInfo(name = "class")
    val className: String,
    val lesson: Int,
    val subject: String?,
    @ColumnInfo(name = "subject_changed")
    val subjectChanged: Boolean,
    val teacher: String?,
    @ColumnInfo(name = "teacher_changed")
    val teacherChanged: Boolean,
    val room: String?,
    @ColumnInfo(name = "room_changed")
    val roomChanged: Boolean,
    val info: String?,
    @ColumnInfo(name = "was_notification_dismissed")
    val wasNotificationDismissed: Boolean,
    @ColumnInfo(name = "sort")
    val sort: Int
) {
    @Transient
    val hasChanges = subjectChanged or teacherChanged or roomChanged or (!info.isNullOrBlank())
}