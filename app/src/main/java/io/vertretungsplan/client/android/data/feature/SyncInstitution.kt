/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import io.vertretungsplan.client.android.data.client.exception.AuthenticationException
import io.vertretungsplan.client.android.data.client.generic.ApiClient
import io.vertretungsplan.client.android.data.dao.getValueCoroutine
import io.vertretungsplan.client.android.data.model.*
import io.vertretungsplan.client.android.data.model.config.ConfigItemKey
import io.vertretungsplan.client.android.data.model.content.FileContentItem
import io.vertretungsplan.client.android.data.model.content.InstitutionContentBucket
import io.vertretungsplan.client.android.data.model.content.MessageContentItem
import io.vertretungsplan.client.android.data.model.content.PlanContent
import io.vertretungsplan.client.android.data.util.ConditionSet
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.util.TaskDeduplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class SyncInstitution (private val registry: AppRegistry) {
    private val sync = TaskDeduplication<String, QueryInstitutionResult>() { institutionId -> syncInternal(institutionId) }

    suspend fun sync(institutionId: String) = sync.handle(institutionId)

    private suspend fun syncInternal(institutionId: String): QueryInstitutionResult {
        val institutionEntry = registry.database.institution().getByIdCoroutine(institutionId)
            ?: throw InstitutionNotFoundException()

        val params = registry.database.institutionParam().queryByInstitutionIdCoroutine(institutionId).associateBy { it.paramId }.mapValues { it.value.value }

        val client = createApiClient(institutionEntry)

        val content = query(
            institutionId = institutionId,
            client = client,
            params = params
        )

        val isCurrentlyOpened = registry.viewedInstitutionReporter.isInstitutionStarted(institutionId)

        saveResponse(
            data = content,
            isCurrentlyOpened = isCurrentlyOpened
        )

        if (!isCurrentlyOpened) {
            registry.notifyInstitution.updateNotificationByInstitutionIdAsync(institutionId)
        }

        registry.clearStorage.requestCleanup()
        registry.backgroundDownload.eventuallyEnqueue()

        return content
    }

    suspend fun createApiClient(institutionEntry: Institution) = registry.apiClientCreator.createApiClient(
        institutionEntry.contentServerUrl
            ?: registry.database.config().getValueCoroutine(ConfigItemKey.LIST_SERVER_URL)
            ?: throw RuntimeException("missing server url")
    )

    suspend fun query(institutionId: String, client: ApiClient, params: Map<String, String>): QueryInstitutionResult {
        var configValidationFailed = false
        val invalidPasswordParams = Collections.synchronizedSet(mutableSetOf<String>())
        val contentBuckets = Collections.synchronizedMap(mutableMapOf<String, InstitutionContentBucket>())
        val planContentBuckets = Collections.synchronizedMap(mutableMapOf<String, PlanContent>())
        val contentBucketsToKeep = Collections.synchronizedSet(mutableSetOf<String>())
        val configBuckets = Collections.synchronizedMap(mutableMapOf<String, InstitutionConfigBucket>())
        val configBucketsToKeep = Collections.synchronizedSet(mutableSetOf<String>())

        coroutineScope {
            fun processContentBucket(bucketId: String, type: ContentBucketSetItemType, passwordParam: String?) {
                if (!contentBucketsToKeep.add(bucketId)) {
                    // bucket was already queried
                    return
                }

                launch (Dispatchers.IO) {
                    try {
                        when (type) {
                            ContentBucketSetItemType.Content -> {
                                val bucketContent = client.queryContentBucket(
                                    institutionId = institutionId,
                                    bucketId = bucketId,
                                    password = params[passwordParam]
                                )

                                contentBuckets[bucketId] = bucketContent
                            }
                            ContentBucketSetItemType.Plan -> {
                                val bucketContent = client.queryPlan(
                                    institutionId = institutionId,
                                    bucketId = bucketId,
                                    password = params[passwordParam]
                                )

                                planContentBuckets[bucketId] = bucketContent
                            }
                        }.let {/* require handling all cases */}
                    } catch (ex: AuthenticationException) {
                        invalidPasswordParams.add(passwordParam)
                    }
                }
            }

            fun processConfigBucket(bucketId: String, passwordParam: String?) {
                if (!configBucketsToKeep.add(bucketId)) {
                    // bucket was already queried
                    return
                }

                launch (Dispatchers.IO) {
                    try {
                        val bucket = client.queryConfigBucket(
                            institutionId = institutionId,
                            bucketId = bucketId,
                            password = params[passwordParam]
                        )

                        fun evaluate(conditionSetId: String) =
                            ConditionSet.evaluate(params, bucket.conditions, conditionSetId)

                        if (!evaluate(bucket.configValidationConditionId)) {
                            configValidationFailed = true
                        }

                        bucket.config
                            .filter { it.type == "otherConfigBucket" }
                            .filter { evaluate(it.visibilityConditionId) }
                            .forEach {
                                processConfigBucket(
                                    bucketId = it.value,
                                    passwordParam = it.param
                                )
                            }

                        bucket.contentBuckets.forEach { contentBucket ->
                            if (evaluate(contentBucket.usageConditionId)) {
                                processContentBucket(contentBucket.id, contentBucket.type, contentBucket.passwordParam)
                            }
                        }

                        configBuckets[bucketId] = bucket
                    } catch (ex: AuthenticationException) {
                        invalidPasswordParams.add(passwordParam)
                    }
                }
            }

            processConfigBucket("default", null)
        }

        return QueryInstitutionResult(
            configValidationFailed = configValidationFailed,
            invalidPasswordParams = invalidPasswordParams,
            contentBuckets = contentBuckets,
            planContentBuckets = planContentBuckets,
            contentBucketsToKeep = contentBucketsToKeep,
            configBuckets = configBuckets,
            configBucketsToKeep = configBucketsToKeep,
            institutionId = institutionId
        )
    }

    private suspend fun saveResponse(data: QueryInstitutionResult, isCurrentlyOpened: Boolean) {
        withContext(Dispatchers.IO) {
            registry.database.runInTransaction {
                data.contentBuckets.entries.forEach { (bucketId, institutionContentBucket) ->
                    syncBucket(
                        institutionId = data.institutionId,
                        contentBucketId = bucketId,
                        bucketContent = institutionContentBucket,
                        isCurrentlyOpened = isCurrentlyOpened
                    )
                }

                data.planContentBuckets.entries.forEach { (bucketId, bucketContent) ->
                    syncBucket(
                        institutionId = data.institutionId,
                        contentBucketId = bucketId,
                        bucketContent = bucketContent,
                        isCurrentlyOpened = isCurrentlyOpened
                    )
                }

                removeDataFromObsoleteBuckets(
                    institutionId = data.institutionId,
                    currentBucketIds = data.contentBucketsToKeep.toList()
                )

                syncConfigBuckets(
                    institutionId = data.institutionId,
                    configBuckets = data.configBuckets.values.toList(),
                    configBucketsToKeep = data.configBucketsToKeep
                )

                val shouldUpdateConfig = data.configValidationFailed or data.invalidPasswordParams.isNotEmpty()
                val institution = registry.database.institution().getByIdSync(data.institutionId)!!

                registry.database.institution().update(
                    institution.copy(
                        shouldUpdateConfig = shouldUpdateConfig
                    )
                )
            }
        }
    }

    private fun syncConfigBuckets(
        institutionId: String,
        configBuckets: List<InstitutionConfigBucket>,
        configBucketsToKeep: Set<String>
    ) {
        registry.database.runInTransaction {
            if (configBuckets.isNotEmpty()) {
                registry.database.institutionConfigDao().upsert(configBuckets)
            }

            val itemsToKeep = configBucketsToKeep + configBuckets.map { it.configBucketId }.toSet()

            if (itemsToKeep.isEmpty()) {
                registry.database.institutionConfigDao().removeByInstitutionId(institutionId)
            } else {
                registry.database.institutionConfigDao().removeOthersByInstitutionId(institutionId, itemsToKeep.toList())
            }
        }
    }

    private fun removeDataFromObsoleteBuckets(
        institutionId: String,
        currentBucketIds: List<String>
    ) {
        registry.database.runInTransaction {
            registry.database.institutionMessageDao().removeMessagesInOtherBuckets(
                institutionId = institutionId,
                contentBucketsToKeep = currentBucketIds
            )

            registry.database.institutionFileDao().removeFilesInOtherBuckets(
                institutionId = institutionId,
                contentBucketsToKeep = currentBucketIds
            )

            registry.database.institutionPlanItemDao().removePlanItemsInOtherBuckets(
                institutionId = institutionId,
                contentBucketsToKeep = currentBucketIds
            )
        }
    }

    private fun syncBucket(
        institutionId: String,
        contentBucketId: String,
        bucketContent: InstitutionContentBucket,
        isCurrentlyOpened: Boolean
    ) {
        registry.database.runInTransaction {
            syncBucketFiles(
                institutionId = institutionId,
                contentBucketId = contentBucketId,
                files = bucketContent.file,
                isCurrentlyOpened = isCurrentlyOpened
            )

            syncBucketMessages(
                institutionId = institutionId,
                contentBucketId = contentBucketId,
                messages = bucketContent.message,
                isCurrentlyOpened = isCurrentlyOpened
            )
        }
    }

    private fun syncBucketMessages(
        institutionId: String,
        contentBucketId: String,
        messages: List<MessageContentItem>,
        isCurrentlyOpened: Boolean
    ) {
        if (messages.map { it.id }.distinct().size != messages.size) {
            throw RuntimeException("file ids must be unique")
        }

        registry.database.runInTransaction {
            val oldMessagesById = registry.database.institutionMessageDao().getMessagesByBucketSync(
                institutionId = institutionId,
                contentBucketId = contentBucketId
            ).associateBy { it.messageId }

            val oldMessageIds = oldMessagesById.keys.toMutableSet()

            messages.forEachIndexed { index, message ->
                val oldMessage = oldMessagesById[message.id]
                oldMessageIds.remove(message.id)

                if (oldMessage != null) {
                    var modifiedMessage = oldMessage.copy(sort = index)

                    if (oldMessage.title != message.title || oldMessage.content != message.content) {
                        modifiedMessage = modifiedMessage.copy(
                            title = message.title,
                            content = message.content
                        )
                    }

                    if (modifiedMessage != oldMessage) {
                        registry.database.institutionMessageDao().update(modifiedMessage)
                    }
                } else {
                    registry.database.institutionMessageDao().insert(InstitutionMessage(
                        institutionId = institutionId,
                        contentBucketId = contentBucketId,
                        messageId = message.id,
                        wasNotificationDismissed = isCurrentlyOpened,
                        notify = message.notify,
                        title = message.title,
                        content = message.content,
                        sort = index
                    ))
                }
            }

            if (oldMessageIds.isNotEmpty()) {
                registry.database.institutionMessageDao().removeMessagesInBucketByIdSync(
                    institutionId = institutionId,
                    contentBucketId = contentBucketId,
                    messageIds = oldMessageIds.toList()
                )
            }
        }
    }

    private fun syncBucketFiles(
        institutionId: String,
        contentBucketId: String,
        files: List<FileContentItem>,
        isCurrentlyOpened: Boolean
    ) {
        if (files.map { it.id }.distinct().size != files.size) {
            throw RuntimeException("file ids must be unique")
        }

        registry.database.runInTransaction {
            val oldFilesById = registry.database.institutionFileDao().getFilesByBucketSync(
                institutionId = institutionId,
                bucketId = contentBucketId
            ).associateBy { it.fileId }
            val oldFileIds = oldFilesById.keys.toMutableSet()

            files.forEachIndexed { index, file ->
                val oldFile = oldFilesById[file.id]
                oldFileIds.remove(file.id)

                if (oldFile != null) {
                    var updatedFile = oldFile.copy(sort = index)

                    val oldFileElements = registry.database.institutionFileElementDao().getByFileIdSync(
                        institutionId = institutionId,
                        contentBucketId = contentBucketId,
                        fileId = file.id
                    )

                    val hasContentChanges = file.file.map { it.sha512 } != oldFileElements.map { it.sha512 }
                    val hasTitleChange = file.title != oldFile.title
                    val hasUrlChanges = file.file.map { it.url } != oldFileElements.map { it.url }

                    if (hasContentChanges) {
                        updatedFile = updatedFile.copy(
                            wasNotificationDismissed = isCurrentlyOpened,
                            wasRead = false
                        )
                    }

                    if (hasTitleChange) {
                        updatedFile = updatedFile.copy(
                            title = file.title
                        )
                    }

                    if (hasContentChanges || hasUrlChanges) {
                        registry.database.institutionFileElementDao().removeByFileId(
                            institutionId = institutionId,
                            contentBucketId = contentBucketId,
                            fileId = file.id
                        )

                        registry.database.institutionFileElementDao().insert(file.file.mapIndexed { index, fileElement ->
                            InstitutionFileElement(
                                institutionId = institutionId,
                                contentBucketId = contentBucketId,
                                fileId = file.id,
                                url = fileElement.url,
                                sha512 = fileElement.sha512,
                                sort = index,
                                size = fileElement.size,
                                scheduleBackgroundDownload = true
                            )
                        })
                    }

                    if (oldFile != updatedFile) {
                        registry.database.institutionFileDao().update(updatedFile)
                    }
                } else {
                    registry.database.institutionFileDao().insert(InstitutionFile(
                        institutionId = institutionId,
                        contentBucketId = contentBucketId,
                        fileId = file.id,
                        type = file.type,
                        title = file.title,
                        mimeType = file.mimeType,
                        lastModified = file.lastModified,
                        notify = file.notify,
                        wasNotificationDismissed = isCurrentlyOpened,
                        wasRead = false,
                        sort = index
                    ))

                    registry.database.institutionFileElementDao().insert(file.file.mapIndexed { index, fileElement ->
                        InstitutionFileElement(
                            institutionId = institutionId,
                            contentBucketId = contentBucketId,
                            fileId = file.id,
                            url = fileElement.url,
                            sha512 = fileElement.sha512,
                            sort = index,
                            size = fileElement.size,
                            scheduleBackgroundDownload = true
                        )
                    })
                }
            }

            if (oldFileIds.isNotEmpty()) {
                registry.database.institutionFileDao().removeFilesInBucketByIdSync(
                    institutionId = institutionId,
                    bucketId = contentBucketId,
                    fileIds = oldFileIds.toList()
                )
            }
        }
    }

    private fun syncBucket(
        institutionId: String,
        contentBucketId: String,
        bucketContent: PlanContent,
        isCurrentlyOpened: Boolean
    ) {
        registry.database.runInTransaction {
            val oldItems = registry.database.institutionPlanItemDao().getPlanItemsByInstitutionAndBucketIdSync(
                institutionId = institutionId,
                contentBucketId = contentBucketId
            ).toMutableList()

            val newItems = mutableListOf<InstitutionPlanItem>()
            val updatedItems = mutableListOf<InstitutionPlanItem>()

            bucketContent.items.forEachIndexed { sort, item ->
                val matchingOldItem = oldItems.find { oldItem ->
                    oldItem.date == item.date &&
                    oldItem.className == item.className &&
                    oldItem.lesson == item.lesson &&
                    oldItem.subject == item.subject &&
                    oldItem.subjectChanged == item.subjectChanged &&
                    oldItem.teacher == item.teacher &&
                    oldItem.teacherChanged == item.teacherChanged &&
                    oldItem.room == item.room &&
                    oldItem.roomChanged == item.roomChanged &&
                    oldItem.info == item.info
                }

                if (matchingOldItem != null) {
                    if (matchingOldItem.sort != sort) {
                        updatedItems.add(
                            matchingOldItem.copy(
                                sort = sort
                            )
                        )
                    }

                    oldItems.remove(matchingOldItem)
                } else {
                    newItems.add(
                        InstitutionPlanItem(
                            id = 0, // auto generate
                            institutionId = institutionId,
                            contentBucketId = contentBucketId,
                            date = item.date,
                            className = item.className,
                            lesson = item.lesson,
                            subject = item.subject,
                            subjectChanged = item.subjectChanged,
                            teacher = item.teacher,
                            teacherChanged = item.teacherChanged,
                            room = item.room,
                            roomChanged = item.roomChanged,
                            info = item.info,
                            wasNotificationDismissed = isCurrentlyOpened,
                            sort = sort
                        )
                    )
                }
            }

            if (newItems.isNotEmpty()) {
                registry.database.institutionPlanItemDao().insert(newItems)
            }

            if (oldItems.isNotEmpty()) {
                registry.database.institutionPlanItemDao().removePlanItemsByIdsSync(oldItems.map { it.id })
            }

            if (updatedItems.isNotEmpty()) {
                registry.database.institutionPlanItemDao().update(updatedItems)
            }
        }
    }
}

data class QueryInstitutionResult(
    val institutionId: String,
    val configValidationFailed: Boolean,
    val invalidPasswordParams: Set<String>,
    val contentBuckets: Map<String, InstitutionContentBucket>,
    val planContentBuckets: Map<String, PlanContent>,
    val contentBucketsToKeep: Set<String>,
    val configBuckets: Map<String, InstitutionConfigBucket>,
    val configBucketsToKeep: Set<String>
)

class InstitutionNotFoundException(): RuntimeException("institution not found")