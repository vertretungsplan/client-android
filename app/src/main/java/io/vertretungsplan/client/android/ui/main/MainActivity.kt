/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.main

import android.Manifest
import android.app.Activity
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.format.DateUtils
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.ScrollableTabRow
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.content.getSystemService
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.model.InstitutionFile
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.AppTheme
import io.vertretungsplan.client.android.ui.about.AboutActivity
import io.vertretungsplan.client.android.ui.config.InstitutionConfigActivity
import io.vertretungsplan.client.android.ui.debug.DebugActivity
import io.vertretungsplan.client.android.ui.openfile.OpenFile
import io.vertretungsplan.client.android.ui.selectinstitution.SelectInstitutionActivity
import kotlinx.coroutines.launch

class MainActivity : FragmentActivity() {
    companion object {
        private const val STATUS_DID_OPEN_FORCE_SELECT_INSTITUTION = "didOpenForceSelectInstitution"
        const val EXTRA_INSTITUTION_ID = "institutionId"
    }

    private val model: MainActivityModel by lazy {
        ViewModelProviders.of(this).get(MainActivityModel::class.java)
    }

    private val requestNotifyPermission = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) model.setBackgroundSyncEnabled(true)
        else Toast.makeText(this, R.string.content_list_bg_sync_permission_toast, Toast.LENGTH_LONG).show()
    }

    private val forceInstitutionSelection = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == Activity.RESULT_OK) didOpenForceSelectInstitutionScreen = false
        else finish()
    }

    private val backDebugCallback = object: OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            startActivity(Intent(this@MainActivity, DebugActivity::class.java))
        }
    }

    init { onBackPressedDispatcher.addCallback(backDebugCallback) }

    private var didOpenForceSelectInstitutionScreen = false
    private var toolbarLongClickCounter = 0

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            didOpenForceSelectInstitutionScreen = savedInstanceState.getBoolean(STATUS_DID_OPEN_FORCE_SELECT_INSTITUTION)
        }

        val registry = DefaultAppRegistry.with(this)
        val selectedInstitutionId = intent.extras?.getString(EXTRA_INSTITUTION_ID)

        model.init(
            registry = registry,
            launchInstitutionId = selectedInstitutionId
        )

        enableEdgeToEdge()

        setContent {
            val content by model.content.observeAsState()

            val institutionTitle = when (val c = content) {
                is DataMainActivityContent -> c.institutionInfo.title
                else -> null
            }

            val isBackgroundSyncEnabled = when (val c = content) {
                is DataMainActivityContent -> c.backgroundSyncEnabled
                else -> null
            }

            val items = when (val c = content) {
                is DataMainActivityContent -> ContentAdapterItemGenerator.buildItems(c)
                else -> emptyList()
            }

            val pagerState = rememberPagerState(pageCount = { items.size })

            AppTheme {
                Scaffold(
                    snackbarHost = { SnackbarHost(model.snackbarHostState) },
                    topBar = {
                        Column {
                            TopAppBar(
                                modifier = Modifier.clickable(
                                    interactionSource = remember { MutableInteractionSource() },
                                    indication = null
                                ) {
                                    toolbarLongClickCounter++

                                    backDebugCallback.isEnabled = toolbarLongClickCounter == 10
                                },
                                title = {
                                    Column {
                                        Text(stringResource(R.string.app_name))

                                        institutionTitle?.let {
                                            Text(
                                                it,
                                                style = MaterialTheme.typography.labelLarge,
                                                maxLines = 1,
                                                overflow = TextOverflow.Ellipsis
                                            )
                                        }
                                    }
                                },
                                actions = {
                                    var showMenu by remember { mutableStateOf(false) }

                                    DropdownMenu(
                                        expanded = showMenu,
                                        onDismissRequest = { showMenu = false }
                                    ) {
                                        DropdownMenuItem(
                                            text = { Text(stringResource(R.string.main_activity_menu_refresh)) },
                                            onClick = {
                                                showMenu = false

                                                model.refresh()
                                            }
                                        )

                                        if (isBackgroundSyncEnabled != null) {
                                            DropdownMenuItem(
                                                text = {
                                                    Row(
                                                        verticalAlignment = Alignment.CenterVertically
                                                    ) {
                                                        Text(stringResource(R.string.main_activity_menu_bg_sync))

                                                        Checkbox(
                                                            checked = isBackgroundSyncEnabled,
                                                            onCheckedChange = {
                                                                showMenu = false

                                                                adjustBackgroundSync(
                                                                    it
                                                                )
                                                            }
                                                        )
                                                    }
                                                },
                                                onClick = {
                                                    showMenu = false

                                                    adjustBackgroundSync(!isBackgroundSyncEnabled)
                                                }
                                            )
                                        }

                                        DropdownMenuItem(
                                            text = { Text(stringResource(R.string.main_activity_menu_select_institution)) },
                                            onClick = {
                                                showMenu = false

                                                startActivity(
                                                    Intent(
                                                        this@MainActivity,
                                                        SelectInstitutionActivity::class.java
                                                    )
                                                )
                                            }
                                        )

                                        DropdownMenuItem(
                                            text = { Text(stringResource(R.string.main_activity_menu_config)) },
                                            onClick = {
                                                showMenu = false

                                                openConfigScreen()
                                            }
                                        )

                                        DropdownMenuItem(
                                            text = { Text(stringResource(R.string.about_title)) },
                                            onClick = {
                                                showMenu = false

                                                startActivity(
                                                    Intent(
                                                        this@MainActivity,
                                                        AboutActivity::class.java
                                                    )
                                                )
                                            }
                                        )
                                    }

                                    Icon(
                                        Icons.Default.MoreVert,
                                        stringResource(R.string.main_activity_menu_open),
                                        Modifier.clickable { showMenu = true }
                                    )
                                }
                            )

                            if (items.size <= 1) {
                                // do nothing
                            } else {
                                val scrollScope = rememberCoroutineScope()

                                val tabs: @Composable () -> Unit = {
                                    var index = 0

                                    for (tab in items) {
                                        val ownIndex = index++

                                        Tab(
                                            selected = index == pagerState.currentPage,
                                            onClick = {
                                                scrollScope.launch {
                                                    pagerState.scrollToPage(ownIndex)
                                                }
                                            }
                                        ) {
                                            Text(
                                                tab.title(LocalContext.current),
                                                Modifier.padding(8.dp)
                                            )
                                        }
                                    }
                                }

                                val selectedTabIndex = pagerState.currentPage
                                    .coerceAtMost(items.size - 1)

                                if (items.size == 2) TabRow(
                                    selectedTabIndex = selectedTabIndex,
                                    tabs = tabs
                                ) else ScrollableTabRow(
                                    selectedTabIndex = selectedTabIndex,
                                    tabs = tabs
                                )
                            }
                        }
                    },
                    content = { insets ->
                        HorizontalPager(
                            state = pagerState,
                            pageContent = { index ->
                                items.getOrNull(index)?.let { page ->
                                    ItemList(page.items, insets)
                                }
                            }
                        )
                    }
                )
            }
        }

        model.content.observe(this, Observer {
            if (it is MissingInstitutionMainActivityContent) {
                if (!didOpenForceSelectInstitutionScreen) {
                    forceInstitutionSelection.launch(
                        Intent(this, SelectInstitutionActivity::class.java)
                    )

                    didOpenForceSelectInstitutionScreen = true
                }
            }
        })
    }

    @Composable private fun ItemList(
        items: List<ContentAdapterItem>,
        padding: PaddingValues
    ) {
        @Composable
        fun MyCard(
            cardModifier: Modifier = Modifier,
            highlight: Boolean = false,
            content: @Composable ColumnScope.() -> Unit
        ) {
            Column (
                Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp, vertical = 4.dp)
            ) {
                Card (
                    Modifier.fillMaxWidth().then(cardModifier),
                    colors = CardDefaults.cardColors(
                        containerColor =
                        if (highlight) MaterialTheme.colorScheme.primaryContainer
                        else Color.Unspecified
                    )
                ) {
                    Column (
                        Modifier.padding(8.dp),
                        verticalArrangement = Arrangement.spacedBy(4.dp),
                        content = content
                    )
                }
            }
        }

        @Composable
        fun MyHeader(text: String) {
            Text(
                text = text,
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp, vertical = 8.dp)
            )
        }

        LazyColumn(
            contentPadding = padding,
            modifier = Modifier.fillMaxSize()
        ) {
            items(items) { item -> when (item) {
                is ShouldUpdateConfigItem -> MyCard (
                    cardModifier = Modifier
                        .clickable { openConfigScreen() }
                ) {
                    Text(
                        stringResource(R.string.content_list_should_config_title),
                        style = MaterialTheme.typography.headlineMedium
                    )

                    Text(
                        stringResource(R.string.content_list_should_config_text)
                    )
                }
                is ContentAdapterHeader -> MyHeader(stringResource(item.titleResourceId))
                is ContentAdapterFileItem -> MyCard(
                    highlight = !item.file.wasRead,
                    cardModifier = Modifier.clickable { onFileClicked(item.file) }
                ) {
                    Text(
                        item.file.title,
                        style = MaterialTheme.typography.headlineSmall
                    )

                    val lastModified = item.file.lastModified

                    val lastModifiedText = if (lastModified != null)
                        stringResource(
                            R.string.content_list_file_last_change,
                            DateUtils.formatDateTime(
                                LocalContext.current,
                                lastModified,
                                DateUtils.FORMAT_SHOW_TIME or DateUtils.FORMAT_SHOW_WEEKDAY
                            )
                        )
                    else
                        stringResource(R.string.content_list_file_last_changed_unknown)

                    Text(lastModifiedText)
                }
                is ContentAdapterDownloadItem -> Text(
                    text = item.file.title,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable { onFileClicked(item.file) }
                        .padding(horizontal = 16.dp, vertical = 4.dp)
                )
                is ContentAdapterMessageItem -> MyCard {
                    Text(
                        item.message.title,
                        style = MaterialTheme.typography.headlineSmall
                    )

                    Text(item.message.content)
                }
                is ContentAdapterPlanHeader -> MyHeader(
                    if (item.skipDate)
                        item.className
                    else
                        stringResource(
                            R.string.content_list_class_header,
                            ContentAdapterItem.formatDate(item.date, LocalContext.current),
                            item.className
                        )
                )
                is ContentAdapterPlanItem -> MyCard {
                    Text(
                        stringResource(
                            R.string.content_list_plan_lesson,
                            item.item.lesson
                        ),
                        style = MaterialTheme.typography.headlineSmall
                    )

                    Text(
                        stringResource(
                            R.string.content_list_plan_subject,
                            item.item.subject ?:
                            stringResource(R.string.content_list_plan_placeholder)
                        ),
                        fontWeight =
                        if (item.item.subjectChanged) FontWeight.Bold
                        else FontWeight.Normal
                    )

                    Text(
                        stringResource(
                            R.string.content_list_plan_room,
                            item.item.room ?:
                            stringResource(R.string.content_list_plan_placeholder)
                        ),
                        fontWeight =
                        if (item.item.roomChanged) FontWeight.Bold
                        else FontWeight.Normal
                    )

                    Text(
                        stringResource(
                            R.string.content_list_plan_teacher,
                            item.item.teacher ?:
                            stringResource(R.string.content_list_plan_placeholder)
                        ),
                        fontWeight =
                        if (item.item.teacherChanged) FontWeight.Bold
                        else FontWeight.Normal
                    )

                    item.item.info?.let { info ->
                        if (info.isNotBlank()) Text(info)
                    }
                }
                is AskForBackgroundSyncItem -> MyCard {
                    Text(
                        stringResource(R.string.content_list_bg_sync_title),
                        style = MaterialTheme.typography.headlineMedium
                    )

                    Text(
                        stringResource(R.string.content_list_bg_sync_text)
                    )

                    Row (
                        Modifier.align(Alignment.End),
                        horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.End)
                    ) {
                        TextButton(
                            onClick = {
                                adjustBackgroundSync(false)
                            },
                            content = {
                                Text(
                                    stringResource(R.string.content_list_bg_sync_no)
                                )
                            }
                        )

                        Button(
                            onClick = {
                                adjustBackgroundSync(true)
                            },
                            content = {
                                Text(
                                    stringResource(R.string.content_list_bg_sync_yes)
                                )
                            }
                        )
                    }
                }
            }}
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean(STATUS_DID_OPEN_FORCE_SELECT_INSTITUTION, didOpenForceSelectInstitutionScreen)
    }

    override fun onStart() {
        super.onStart()

        model.reportOnStart()

        toolbarLongClickCounter = 0
        backDebugCallback.isEnabled = false
    }

    override fun onStop() {
        super.onStop()

        if (isChangingConfigurations) {
            model.ignoreOneStart()
        }
    }

    private fun onFileClicked(file: InstitutionFile) {
        OpenFile.openFileFromContentList(
            file = file,
            fragmentManager = supportFragmentManager
        )
    }

    private fun openConfigScreen() {
        val content = model.content.value

        if (content is DataMainActivityContent) {
            startActivity(
                Intent(this, InstitutionConfigActivity::class.java)
                    .putExtra(InstitutionConfigActivity.EXTRA_INSTITUTION_ID, content.institutionInfo.id)
            )
        } else {
            // user is redirected automatically
        }
    }

    private fun adjustBackgroundSync(enable: Boolean) {
        if (enable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                val hasPermission = getSystemService<NotificationManager>()!!.areNotificationsEnabled()

                if (hasPermission) model.setBackgroundSyncEnabled(true)
                else requestNotifyPermission.launch(Manifest.permission.POST_NOTIFICATIONS)
            } else {
                model.setBackgroundSyncEnabled(true)
            }
        } else model.setBackgroundSyncEnabled(false)
    }
}
