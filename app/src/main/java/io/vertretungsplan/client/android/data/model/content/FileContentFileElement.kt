/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model.content

import android.util.JsonReader
import io.vertretungsplan.client.android.data.HashValidation

data class FileContentFileElement(
    val url: String,
    val sha512: String,
    val size: Long
) {
    companion object {
        fun parse(reader: JsonReader): FileContentFileElement {
            var url: String? = null
            var sha512: String? = null
            var size: Long? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "url" -> url = reader.nextString()
                    "sha512" -> sha512 = reader.nextString()
                    "size" -> size = reader.nextLong()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return FileContentFileElement(
                url = url!!,
                sha512 = sha512!!,
                size = size!!
            )
        }
    }

    init {
        HashValidation.assertValidSha512(sha512)
    }
}