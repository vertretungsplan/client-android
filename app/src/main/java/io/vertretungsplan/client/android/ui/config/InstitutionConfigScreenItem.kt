/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2021 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.config

import io.vertretungsplan.client.android.data.model.ConfigurationScreenItem

sealed class InstitutionConfigScreenItem {
    abstract val id: Long

    companion object {
        // returns null on errors
        fun parse(id: Long, item: ConfigurationScreenItem, params: Map<String, String>, invalidPasswords: Set<String>): InstitutionConfigScreenItem? = when (item.type) {
            "radio" -> RadioButtonItem(
                id = id,
                param = item.param,
                value = item.value,
                label = item.label,
                isChecked = params[item.param] == item.value
            )
            "password" -> PasswordItem(
                id = id,
                param = item.param,
                value = params[item.param] ?: "",
                hint = item.label,
                isInvalid = invalidPasswords.contains(item.param)
            )
            "text" -> TextItem(
                id = id,
                text = item.label
            )
            "divider" -> DividerItem(
                id = id
            )
            else -> null
        }
    }
}

data class RadioButtonItem(override val id: Long, val param: String, val value: String, val label: String, val isChecked: Boolean): InstitutionConfigScreenItem()
data class PasswordItem(override val id: Long, val param: String, val value: String, val hint: String, val isInvalid: Boolean): InstitutionConfigScreenItem()
data class MissingOptionsItem(override val id: Long, val textResource: Int): InstitutionConfigScreenItem()
data class TextItem(override val id: Long, val text: String): InstitutionConfigScreenItem()
data class DividerItem(override val id: Long): InstitutionConfigScreenItem()