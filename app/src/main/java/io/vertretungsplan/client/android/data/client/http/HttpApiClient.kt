/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.client.http

import io.vertretungsplan.client.android.data.client.generic.ApiClient
import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.data.model.InstitutionConfigBucket
import io.vertretungsplan.client.android.data.model.content.InstitutionContentBucket
import io.vertretungsplan.client.android.data.model.content.PlanContent
import io.vertretungsplan.client.android.data.util.parseList
import okhttp3.OkHttpClient
import okhttp3.Request

class HttpApiClient(serverUrl: String, private val httpClient: OkHttpClient): ApiClient {
    private val serverUrlWithoutSlashAtEnd = serverUrl.replace(Regex("/*$"), "")

    override fun queryInstitutionList(): List<Institution> {
        httpClient.newCall(
            Request.Builder()
                .url("$serverUrlWithoutSlashAtEnd/vp-content")
                .build()
        ).execute().parseJson { reader ->
            var list: List<Institution>? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "institutions" -> list = reader.parseList { Institution.parse(reader) }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return list!!
        }
    }

    override fun queryConfigBucket(
        institutionId: String,
        bucketId: String,
        password: String?
    ): InstitutionConfigBucket {
        httpClient.newCall(
            Request.Builder()
                .url("$serverUrlWithoutSlashAtEnd/vp-institution/$institutionId/config/$bucketId")
                .addBasicAuth(password)
                .build()
        ).execute().parseJson { reader ->
            return InstitutionConfigBucket.parse(
                reader = reader,
                institutionId = institutionId,
                configBucketId = bucketId
            )
        }
    }

    override fun queryContentBucket(
        institutionId: String,
        bucketId: String,
        password: String?
    ): InstitutionContentBucket {
        httpClient.newCall(
            Request.Builder()
                .url("$serverUrlWithoutSlashAtEnd/vp-institution/$institutionId/content/$bucketId")
                .addBasicAuth(password)
                .build()
        ).execute().parseJson { reader ->
            return InstitutionContentBucket.parse(
                reader = reader
            )
        }
    }

    override fun queryPlan(
        institutionId: String,
        bucketId: String,
        password: String?
    ): PlanContent {
        httpClient.newCall(
            Request.Builder()
                .url("$serverUrlWithoutSlashAtEnd/vp-institution/$institutionId/plan/$bucketId")
                .addBasicAuth(password)
                .build()
        ).execute().parseJson { reader ->
            return PlanContent.parse(
                reader = reader
            )
        }
    }
}