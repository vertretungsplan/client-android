/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.config

import android.app.Activity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.NavigateNext
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.AppTheme
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.launch

class InstitutionConfigActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_INSTITUTION_ID = "institutionId"
    }

    @OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val registry = DefaultAppRegistry.with(this)

        val institutionId = intent.getStringExtra(EXTRA_INSTITUTION_ID)!!
        val model = ViewModelProviders.of(this).get(InstitutionConfigModel::class.java)

        model.init(
            institutionId = institutionId,
            registry = registry
        )

        model.institution.observe(this) {
            if (it == null) {
                finish()
            }
        }

        lifecycleScope.launch {
            model.commandChannel.consumeEach { command ->
                when (command) {
                    is InstitutionConfigModel.ActivityCommand.Finish -> {
                        if (command.ok) setResult(Activity.RESULT_OK)

                        finish()
                    }
                }
            }
        }

        enableEdgeToEdge()

        setContent {
            val isBusy by model.isBusy.observeAsState(true)
            val institution by model.institution.observeAsState()
            val options by model.optionsLive.observeAsState(emptyList())

            AppTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Column {
                                    Text(stringResource(R.string.app_name))

                                    institution?.title?.let {
                                        Text(
                                            it,
                                            style = MaterialTheme.typography.labelLarge,
                                            maxLines = 1,
                                            overflow = TextOverflow.Ellipsis
                                        )
                                    }
                                }
                            }
                        )
                    },
                    floatingActionButton = {
                        if (!isBusy) FloatingActionButton(
                            onClick = {
                                model.sync()
                            },
                            content = {
                                Icon(
                                    Icons.AutoMirrored.Filled.NavigateNext,
                                    stringResource(R.string.config_action_save)
                                )
                            }
                        )
                    },
                    snackbarHost = { SnackbarHost(model.snackbar) },
                    content = { insets ->
                        if (isBusy) Box(
                            Modifier.fillMaxSize().padding(insets).padding(16.dp),
                            content = {
                                CircularProgressIndicator(
                                    Modifier.align(Alignment.Center)
                                )
                            }
                        ) else LazyColumn(
                            contentPadding = insets,
                            modifier = Modifier.fillMaxSize()
                        ) {
                            items(options, key = { it.id }) { option -> when (option) {
                                is RadioButtonItem -> Row(
                                    modifier = Modifier
                                        .animateItemPlacement()
                                        .fillMaxWidth()
                                        .clickable { model.updateParam(option.param, option.value) }
                                        .padding(horizontal = 8.dp, vertical = 4.dp),
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    RadioButton(
                                        selected = option.isChecked,
                                        onClick = { model.updateParam(option.param, option.value) }
                                    )

                                    Text(
                                        option.label,
                                        Modifier.weight(1f)
                                    )
                                }
                                is PasswordItem -> TextField(
                                    value = option.value,
                                    onValueChange = { value: String -> model.updateParam(option.param, value) },
                                    label = { Text(option.hint) },
                                    singleLine = true,
                                    visualTransformation = PasswordVisualTransformation(),
                                    keyboardOptions = KeyboardOptions(
                                        keyboardType = KeyboardType.Password,
                                        imeAction = ImeAction.Done
                                    ),
                                    modifier = Modifier
                                        .animateItemPlacement()
                                        .fillMaxWidth()
                                )
                                is MissingOptionsItem -> Text(
                                    stringResource(option.textResource),
                                    Modifier
                                        .animateItemPlacement()
                                        .padding(horizontal = 16.dp, vertical = 8.dp)
                                )
                                is TextItem -> Text(
                                    option.text,
                                    Modifier
                                        .animateItemPlacement()
                                        .padding(horizontal = 16.dp, vertical = 8.dp)
                                )
                                is DividerItem -> HorizontalDivider(
                                    Modifier.animateItemPlacement()
                                )
                            }}
                        }
                    }
                )
            }
        }
    }
}
