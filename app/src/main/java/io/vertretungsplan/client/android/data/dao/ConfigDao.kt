/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.vertretungsplan.client.android.data.model.config.ConfigItem

@Dao
interface ConfigDao {
    @Query("SELECT * FROM config WHERE `key` = :key")
    fun getConfigItemLive(key: String): LiveData<ConfigItem?>

    @Query("SELECT * FROM config WHERE `key` = :key")
    fun getConfigItemSync(key: String): ConfigItem?

    @Query("SELECT * FROM config WHERE `key` = :key")
    suspend fun getConfigItemCoroutine(key: String): ConfigItem?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertConfigItemSync(item: ConfigItem)

    @Query("DELETE FROM config WHERE `key` = :key")
    fun removeConfigItemSync(key: String)
}

fun ConfigDao.setValueSync(key: String, value: String?) {
    if (value != null) {
        upsertConfigItemSync(ConfigItem(key, value))
    } else {
        removeConfigItemSync(key)
    }
}

fun ConfigDao.getValueLive(key: String) = getConfigItemLive(key).map { it?.value }
fun ConfigDao.getValueSync(key: String) = getConfigItemSync(key)?.value
suspend fun ConfigDao.getValueCoroutine(key: String) = getConfigItemCoroutine(key)?.value