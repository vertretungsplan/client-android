/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import io.vertretungsplan.client.android.data.model.InstitutionFile

@Dao
interface InstitutionFileDao {
    @Insert
    fun insert(item: InstitutionFile)

    @Update
    fun update(item: InstitutionFile)

    @Query("UPDATE institution_file SET was_read = 1 WHERE institution_id = :institutionId AND content_bucket_id = :bucketId AND file_id = :fileId")
    fun setFileReadSync(institutionId: String, bucketId: String, fileId: String)

    @Query("SELECT * FROM institution_file WHERE institution_id = :institutionId AND content_bucket_id = :bucketId")
    fun getFilesByBucketSync(institutionId: String, bucketId: String): List<InstitutionFile>

    @Query("SELECT * FROM institution_file WHERE institution_id = :institutionId ORDER BY sort ASC")
    fun getFilesByInstitutionLive(institutionId: String): LiveData<List<InstitutionFile>>

    @Query("SELECT * FROM institution_file WHERE institution_id = :institutionId AND content_bucket_id = :bucketId AND file_id = :fileId")
    fun getFileByInstitutionAndIdLive(institutionId: String, bucketId: String, fileId: String): LiveData<InstitutionFile?>

    @Query("SELECT * FROM institution_file WHERE institution_id = :institutionId AND content_bucket_id = :bucketId AND file_id = :fileId")
    fun getFileByInstitutionAndIdSync(institutionId: String, bucketId: String, fileId: String): InstitutionFile?

    @Query("DELETE FROM institution_file WHERE institution_id = :institutionId AND content_bucket_id = :bucketId AND file_id IN (:fileIds)")
    fun removeFilesInBucketByIdSync(institutionId: String, bucketId: String, fileIds: List<String>)

    @Query("DELETE FROM institution_file WHERE institution_id = :institutionId AND content_bucket_id NOT IN (:contentBucketsToKeep)")
    fun removeFilesInOtherBuckets(institutionId: String, contentBucketsToKeep: List<String>)

    @Query("UPDATE institution_file SET was_notification_dismissed = 1 WHERE institution_id = :institutionId")
    fun setNotificationDismissedByInstitutionIdSync(institutionId: String)

    @Query("SELECT * FROM institution_file WHERE institution_id = :institutionId AND was_notification_dismissed = 0 AND notify = 1 AND type = \"plan\" ORDER BY sort ASC")
    fun getFilesForNotificationSync(institutionId: String): List<InstitutionFile>
}