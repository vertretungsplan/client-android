/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.registry.AppRegistry
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class BackgroundSync (private val registry: AppRegistry) {
    companion object {
        private const val LOG_TAG = "BackgroundSync"
    }

    suspend fun doBackgroundSync() {
        val institutions = registry.database.backgroundSyncInstitution().getInstitutionsWithBackgroundSync()

        coroutineScope {
            if (institutions.isNotEmpty()) {
                try {
                    registry.syncInstitutionList.syncIfLastSyncLongAgo()
                } catch (ex: Exception) {
                    if (BuildConfig.DEBUG) {
                        Log.w(LOG_TAG, "could not do background sync of the institution list", ex)
                    }
                }

                institutions.forEach { institution ->
                    launch {
                        try {
                            registry.syncInstitution.sync(institution.institutionId)
                        } catch (ex: Exception) {
                            if (BuildConfig.DEBUG) {
                                Log.w(LOG_TAG, "could not do background sync", ex)
                            }
                        }
                    }
                }
            }
        }
    }
}