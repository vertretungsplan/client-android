/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.TypeConverters
import io.vertretungsplan.client.android.data.converter.FileContentItemTypeConverter
import io.vertretungsplan.client.android.data.model.content.FileContentItemType

@Entity(
    tableName = "institution_file",
    primaryKeys = [
        "institution_id",
        "content_bucket_id",
        "file_id"
    ],
    foreignKeys = [
        ForeignKey(
            entity = Institution::class,
            parentColumns = ["id"],
            childColumns = ["institution_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
@TypeConverters(FileContentItemTypeConverter::class)
data class InstitutionFile(
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "content_bucket_id")
    val contentBucketId: String,
    @ColumnInfo(name = "file_id")
    val fileId: String,
    val type: FileContentItemType,
    val title: String,
    val mimeType: String,
    val lastModified: Long?,
    val notify: Boolean,
    @ColumnInfo(name = "was_notification_dismissed")
    val wasNotificationDismissed: Boolean,
    @ColumnInfo(name = "was_read")
    val wasRead: Boolean,
    @ColumnInfo(name = "sort")
    val sort: Int
) {
    @Transient
    val primaryKey = (institutionId + contentBucketId + fileId)
}