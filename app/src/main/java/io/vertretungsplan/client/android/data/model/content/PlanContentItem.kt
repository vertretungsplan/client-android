/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model.content

import android.util.JsonReader
import io.vertretungsplan.client.android.data.util.nextStringOrNull

data class PlanContentItem(
    val date: String,
    val className: String,
    val lesson: Int,
    val subject: String?,
    val subjectChanged: Boolean,
    val teacher: String?,
    val teacherChanged: Boolean,
    val room: String?,
    val roomChanged: Boolean,
    val info: String?
) {
    companion object {
        fun parse(reader: JsonReader): PlanContentItem {
            var date: String? = null
            var className: String? = null
            var lesson: Int? = null
            var subject: String? = null
            var subjectChanged: Boolean? = null
            var teacher: String? = null
            var teacherChanged: Boolean? = null
            var room: String? = null
            var roomChanged: Boolean? = null
            var info: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "date" -> date = reader.nextString()
                    "class" -> className = reader.nextString()
                    "lesson" -> lesson = reader.nextInt()
                    "subject" -> subject = reader.nextStringOrNull()
                    "subjectChanged" -> subjectChanged = reader.nextBoolean()
                    "teacher" -> teacher = reader.nextStringOrNull()
                    "teacherChanged" -> teacherChanged = reader.nextBoolean()
                    "room" -> room = reader.nextStringOrNull()
                    "roomChanged" -> roomChanged = reader.nextBoolean()
                    "info" -> info = reader.nextStringOrNull()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return PlanContentItem(
                date = date!!,
                className = className!!,
                lesson = lesson!!,
                subject = subject,
                subjectChanged = subjectChanged!!,
                teacher = teacher,
                teacherChanged = teacherChanged!!,
                room = room,
                roomChanged = roomChanged!!,
                info = info
            )
        }
    }
}