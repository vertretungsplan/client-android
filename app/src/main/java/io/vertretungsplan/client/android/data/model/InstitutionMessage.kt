/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "institution_message",
    primaryKeys = [
        "institution_id",
        "content_bucket_id",
        "message_id"
    ],
    foreignKeys = [
        ForeignKey(
            entity = Institution::class,
            parentColumns = ["id"],
            childColumns = ["institution_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class InstitutionMessage(
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "content_bucket_id")
    val contentBucketId: String,
    @ColumnInfo(name = "message_id")
    val messageId: String,
    val notify: Boolean,
    @ColumnInfo(name = "was_notification_dismissed")
    val wasNotificationDismissed: Boolean,
    val title: String,
    val content: String,
    @ColumnInfo(name = "sort")
    val sort: Int
) {
    @Transient
    val primaryKey = (institutionId + contentBucketId + messageId)
}