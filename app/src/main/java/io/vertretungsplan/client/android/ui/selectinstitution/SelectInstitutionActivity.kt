/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.selectinstitution

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.AppTheme
import io.vertretungsplan.client.android.ui.selectserver.SelectServerActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SelectInstitutionActivity : FragmentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val registry = DefaultAppRegistry(this)
        val model = ViewModelProviders.of(this).get(SelectInstitutionModel::class.java)

        val forceServerSelection = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode == Activity.RESULT_OK) model.updateList()
            else {
                // can not operate => close screen
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }

        model.init(registry)

        enableEdgeToEdge()

        setContent {
            val scope = rememberCoroutineScope()
            val status by model.status.observeAsState()
            val listContent by model.listContent.observeAsState()
            val snackbar = SnackbarHostState()

            fun onInstitutionClicked(item: Institution) {
                scope.launch {
                    withContext(Dispatchers.IO) {
                        model.setSelectionSync(item.id)
                    }

                    withContext(Dispatchers.Main) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }
            }

            if (status == SelectInstitutionModel.Mode.ReadyWithFailedUpdate) {
                LaunchedEffect(true) {
                    snackbar.showSnackbar(
                        getString(R.string.select_institution_failed_update),
                        duration = SnackbarDuration.Indefinite
                    )
                }
            }

            if (status == SelectInstitutionModel.Mode.NeedsListServer) {
                LaunchedEffect(true) {
                    forceServerSelection.launch(Intent(this@SelectInstitutionActivity, SelectServerActivity::class.java))
                }
            }

            val showList = remember(status) {
                status == SelectInstitutionModel.Mode.Ready ||
                        status == SelectInstitutionModel.Mode.ReadyWithFailedUpdate
            }

            AppTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = { Text(stringResource(R.string.app_name)) }
                        )
                    },
                    snackbarHost = { SnackbarHost(snackbar) },
                    content = { paddingValues ->
                        if (showList) LazyColumn(
                            contentPadding = paddingValues,
                            modifier = Modifier.fillMaxSize()
                        ) {
                            items (listContent ?: emptyList(), key = { it.id }) {
                                Box(
                                    Modifier
                                        .fillMaxWidth()
                                        .clickable { onInstitutionClicked(it) }
                                        .padding(horizontal = 16.dp, vertical = 8.dp)
                                ) {
                                    Text(it.title)
                                }
                            }
                        } else Box(Modifier.fillMaxSize().padding(paddingValues).padding(8.dp)) {
                            CircularProgressIndicator(Modifier.align(Alignment.Center))
                        }
                    }
                )
            }
        }
    }
}
