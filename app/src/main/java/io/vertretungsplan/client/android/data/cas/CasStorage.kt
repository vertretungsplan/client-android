/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.cas

import io.vertretungsplan.client.android.data.HashValidation
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.File
import java.io.IOException
import java.util.*

class CasStorage (val baseDirectory: File) {
    fun getFileByHash(hash: String): File {
        HashValidation.assertValidSha512(hash)

        return File(File(baseDirectory, hash.substring(0, 2)), hash.substring(2))
    }

    private val tempFileDirectory = File(baseDirectory, "temp")
    private val initTempLock = Object()
    private var didInitTempDirectory = false

    fun initTempDirectory() {
        if (didInitTempDirectory) {
            tempFileDirectory.mkdirs()

            if (!tempFileDirectory.isDirectory) {
                throw IOException("could not create temp directory")
            }
        } else {
            synchronized(initTempLock) {
                if (!didInitTempDirectory) {
                    if (!tempFileDirectory.deleteRecursively()) {
                        throw IOException("could not clean up temp directory")
                    }

                    if (!tempFileDirectory.mkdirs()) {
                        throw IOException("could not create temp directory")
                    }

                    didInitTempDirectory = true
                }
            }
        }
    }

    fun createNewTempFile(): File {
        initTempDirectory()

        return File(tempFileDirectory, UUID.randomUUID().toString())
    }

    fun getAllHashes(): Flow<String> {
        return flow {
            baseDirectory.listFiles()
                ?.filter { file -> file.isDirectory and (file.name.length == 2) }
                ?.forEach { directory ->
                    directory.list()?.forEach { file ->
                        val hash = directory.name + file

                        if (HashValidation.isValidSha512(hash)) {
                            emit(hash)
                        }
                    }
                }
        }
    }
}