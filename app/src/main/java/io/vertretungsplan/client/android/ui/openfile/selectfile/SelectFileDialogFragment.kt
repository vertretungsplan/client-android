/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.selectfile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.vertretungsplan.client.android.data.model.InstitutionFile
import io.vertretungsplan.client.android.data.model.InstitutionFileElement
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.openfile.download.DownloadFileDialogFragment

class SelectFileDialogFragment: BottomSheetDialogFragment() {
    companion object {
        private const val ARG_INSTITUTION_ID = "institution_id"
        private const val ARG_BUCKET_ID = "bucketId"
        private const val ARG_FILE_ID = "fileId"
        private const val DIALOG_TAG = "SelectFileDialogFragment"

        fun newInstance(institutionId: String, bucketId: String, fileId: String) = SelectFileDialogFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_INSTITUTION_ID, institutionId)
                putString(ARG_BUCKET_ID, bucketId)
                putString(ARG_FILE_ID, fileId)
            }
        }
    }

    private val model: SelectFileDialogModel by lazy { ViewModelProviders.of(this).get(SelectFileDialogModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val registry = DefaultAppRegistry.with(context!!)

        model.init(
            institutionId = arguments!!.getString(ARG_INSTITUTION_ID)!!,
            bucketId = arguments!!.getString(ARG_BUCKET_ID)!!,
            fileId = arguments!!.getString(ARG_FILE_ID)!!,
            registry = registry
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val recycler = RecyclerView(context!!)
        val adapter = SelectFileAdapter()

        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter

        fun openDownloadScreen(file: InstitutionFile, fileElement: InstitutionFileElement) {
            DownloadFileDialogFragment.newInstance(
                institutionId = file.institutionId,
                contentBucketId = file.contentBucketId,
                fileId = file.fileId,
                title = file.title,
                mimeType = file.mimeType,
                sha512 = fileElement.sha512,
                url = fileElement.url,
                size = fileElement.size
            ).show(fragmentManager!!)
        }

        adapter.listener = object: SelectFileAdapterListener {
            override fun onFileClicked(fileElement: InstitutionFileElement) {
                model.content.value?.first?.let { file ->
                    openDownloadScreen(file, fileElement)
                }

                dismiss()
            }
        }

        model.content.observe(this, Observer { content ->
            if (content == null) {
                dismiss()
            } else if (content.second.isEmpty()) {
                Toast.makeText(context!!, "illegal state", Toast.LENGTH_SHORT).show()
                dismiss()
            } else if (content.second.size == 1) {
                val file = content.first
                val fileElement = content.second.single()

                openDownloadScreen(file, fileElement)

                dismiss()
            } else {
                adapter.items = content.second
            }
        })

        return recycler
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}