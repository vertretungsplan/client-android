/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import androidx.fragment.app.FragmentManager
import io.vertretungsplan.client.android.data.CasProvider
import io.vertretungsplan.client.android.data.model.InstitutionFile
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.openfile.selectfile.SelectFileDialogFragment
import io.vertretungsplan.client.android.ui.openfile.unsupported.MissingPdfReaderDialogFragment
import io.vertretungsplan.client.android.ui.openfile.unsupported.UnsupportedFileDialogFragment
import io.vertretungsplan.client.android.ui.viewer.html.HtmlViewerActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object OpenFile {
    fun openFileFromContentList(
        file: InstitutionFile,
        fragmentManager: FragmentManager
    ) {
        SelectFileDialogFragment.newInstance(
            institutionId = file.institutionId,
            bucketId = file.contentBucketId,
            fileId = file.fileId
        ).show(fragmentManager)
    }

    fun openDownloadedFile(
        context: Context,
        fragmentManager: FragmentManager,
        institutionId: String,
        contentBucketId: String,
        fileId: String,
        title: String,
        mimeType: String,
        sha512: String
    ) {
        val registry = DefaultAppRegistry.with(context)

        val uri = CasProvider.createFileUri(
            sha512 = sha512,
            title = title,
            mimeType = mimeType
        )

        val baseMimeType = context.contentResolver.getType(uri)

        fun markFileRead() {
            GlobalScope.launch (Dispatchers.IO) {
                registry.database.institutionFileDao().setFileReadSync(
                    institutionId = institutionId,
                    bucketId = contentBucketId,
                    fileId = fileId
                )
            }
        }

        if (baseMimeType == "text/html") {
            context.startActivity(
                Intent(context, HtmlViewerActivity::class.java)
                    .setData(uri)
            )

            markFileRead()
        } else {
            try {
                context.startActivity(
                    Intent(Intent.ACTION_VIEW, uri)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                )

                markFileRead()
            } catch (ex: ActivityNotFoundException) {
                if (baseMimeType == "application/pdf") {
                    MissingPdfReaderDialogFragment().show(fragmentManager)
                } else {
                    UnsupportedFileDialogFragment.newInstance(baseMimeType ?: mimeType).show(fragmentManager)
                }
            }
        }
    }
}