/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.dao.getValueSync
import io.vertretungsplan.client.android.data.dao.setValueSync
import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.data.model.config.ConfigItemKey
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.util.TaskDeduplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SyncInstitutionList (private val registry: AppRegistry) {
    companion object {
        private const val LOG_TAG = "SyncInstitutionList"
    }

    private val deduplication = TaskDeduplication<Void?, Unit>() { _ -> syncInternal() }

    fun syncIfLastSyncLongAgoAsync() {
        GlobalScope.launch {
            try {
                syncIfLastSyncLongAgo()
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "syncIfLastSyncLongAgoAsync() failed", ex)
                }
            }
        }
    }

    suspend fun syncIfLastSyncLongAgo() {
        withContext(Dispatchers.IO) {
            val lastSyncTimestamp = registry.database.config().getValueSync(ConfigItemKey.LAST_INSTITUTION_LIST_SYNC)?.toLongOrNull(10)
            val now = System.currentTimeMillis()

            val missingLastSync = lastSyncTimestamp == null
            val lastSyncInTheFuture = lastSyncTimestamp != null && lastSyncTimestamp > now
            val lastSyncMoreThanOneDayAgo = lastSyncTimestamp != null && lastSyncTimestamp < (now - 1000 * 60 * 60 * 24)
            val shouldSyncNow = missingLastSync or lastSyncInTheFuture or lastSyncMoreThanOneDayAgo

            if (shouldSyncNow) {
                forceSync()
            }
        }
    }

    suspend fun forceSync() {
        deduplication.handle(null)
    }

    private fun syncInternal() {
        val serverUrl = registry.database.config().getValueSync(ConfigItemKey.LIST_SERVER_URL)!!
        val client = registry.apiClientCreator.createApiClient(serverUrl)
        val content = client.queryInstitutionList()

        registry.database.runInTransaction {
            sync(content)

            registry.database.config().setValueSync(ConfigItemKey.LAST_INSTITUTION_LIST_SYNC, System.currentTimeMillis().toString(10))
        }
    }

    private fun sync(content: List<Institution>) {
        registry.database.runInTransaction {
            content.forEach { item ->
                val oldItem = registry.database.institution().getByIdSync(item.id)

                if (oldItem != null) {
                    registry.database.institution().update(item)
                } else {
                    registry.database.institution().insert(item)
                }
            }

            if (content.isNotEmpty()) {
                registry.database.institution().deleteOtherIds(content.map { it.id })
            } else {
                registry.database.institution().deleteAll()
            }
        }
    }
}