/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.TypeConverter
import io.vertretungsplan.client.android.data.util.parseStringMap
import io.vertretungsplan.client.android.data.util.useJsonReader
import io.vertretungsplan.client.android.data.util.useJsonWriter

data class ConditionSetItem(
    val id: String,
    val type: String,
    val left: String,
    val right: String
) {
    companion object {
        private const val ID = "id"
        private const val TYPE = "type"
        private const val LEFT = "left"
        private const val RIGHT = "right"

        fun parse(reader: JsonReader): ConditionSetItem {
            var id: String? = null
            var type: String? = null
            var left: String? = null
            var right: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextString()
                    TYPE -> type = reader.nextString()
                    LEFT -> left = reader.nextString()
                    RIGHT -> right = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ConditionSetItem(
                id = id!!,
                type = type!!,
                left = left!!,
                right = right!!
            )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ID).value(id)
        writer.name(TYPE).value(type)
        writer.name(LEFT).value(left)
        writer.name(RIGHT).value(right)

        writer.endObject()
    }
}

class ConditionSetItemMapConverter {
    @TypeConverter
    fun fromString(value: String): Map<String, ConditionSetItem>  = value.useJsonReader { json ->
        json.parseStringMap { ConditionSetItem.parse(json) }
    }

    @TypeConverter
    fun serialize(value: Map<String, ConditionSetItem>): String = useJsonWriter { json ->
        json.beginObject()
        value.entries.forEach { entry ->
            json.name(entry.key)
            entry.value.serialize(json)
        }
        json.endObject()
    }
}