/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.viewer.html

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SmallFloatingActionButton
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.fragment.app.FragmentActivity
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.CasProvider
import io.vertretungsplan.client.android.registry.DefaultAppRegistry
import io.vertretungsplan.client.android.ui.AppTheme
import okhttp3.MediaType.Companion.toMediaTypeOrNull

class HtmlViewerActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableEdgeToEdge()

        setContent {
            AppTheme {
                Scaffold(
                    content = { insets ->
                        Box(Modifier.padding(insets)) {
                            AndroidView(
                                modifier = Modifier.fillMaxSize(),
                                factory = {
                                    WebView(it).also { webview ->
                                        webview.settings.apply {
                                            allowFileAccess = false
                                            allowContentAccess = true
                                            javaScriptEnabled = false
                                            builtInZoomControls = false
                                            setSupportZoom(false)
                                            blockNetworkImage = true
                                            blockNetworkLoads = true
                                        }

                                        webview.webViewClient = object : WebViewClient() {
                                            override fun shouldInterceptRequest(
                                                view: WebView?,
                                                url: String?
                                            ): WebResourceResponse? {
                                                // workaround for old android versions

                                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M && url != null && url.startsWith(
                                                        CasProvider.BASE
                                                    )
                                                ) {
                                                    val uri = Uri.parse(url)
                                                    val hash =
                                                        uri.getQueryParameter(CasProvider.URL_PARAM_SHA512)!!
                                                    val type =
                                                        uri.getQueryParameter(CasProvider.URL_PARAM_MIME_TYPE)!!
                                                    val file =
                                                        DefaultAppRegistry.with(this@HtmlViewerActivity).cas.getFileByHash(
                                                            hash
                                                        )

                                                    val mediaType = type.toMediaTypeOrNull()

                                                    if (mediaType != null) {
                                                        return WebResourceResponse(
                                                            "${mediaType.type}/${mediaType.subtype}",
                                                            mediaType.parameter("charset"),
                                                            file.inputStream()
                                                        )
                                                    }
                                                }

                                                return super.shouldInterceptRequest(view, url)
                                            }
                                        }

                                        webview.loadUrl(intent.data!!.toString())
                                    }
                                }
                            )

                            SmallFloatingActionButton(
                                onClick = { finish() },
                                modifier = Modifier.padding(16.dp),
                                content = {
                                    Icon(
                                        Icons.AutoMirrored.Filled.ArrowBack,
                                        stringResource(R.string.viewer_action_back)
                                    )
                                }
                            )
                        }
                    }
                )
            }
        }
    }
}
