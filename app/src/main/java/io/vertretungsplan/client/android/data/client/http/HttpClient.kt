/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2021 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.client.http

import android.util.JsonReader
import android.util.Log
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.client.exception.AuthenticationException
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException

object HttpClient {
    val instance = OkHttpClient.Builder()
        .sslSocketFactory(SslConfig.certificates.sslSocketFactory(), SslConfig.certificates.trustManager)
        .let { builder ->
            if (BuildConfig.DEBUG) {
                builder.addInterceptor (HttpLoggingInterceptor {
                    Log.d("HttpClient", it)
                })
            }

            builder.build()
        }
}

fun Request.Builder.addBasicAuth(password: String?): Request.Builder {
    if (!password.isNullOrEmpty()) {
        this.addHeader("Authorization", Credentials.basic("", password))
    }

    return this
}

fun Response.assertSuccess() {
    if (!this.isSuccessful) {
        if (this.code == 401) {
            throw AuthenticationException()
        }

        throw IOException("http request failed: ${this.code}")
    }
}

inline fun <R> Response.parseJson(parser: (JsonReader) -> R): R {
    use {
        it.assertSuccess()

        JsonReader(it.body!!.charStream()).use { reader ->
            return parser(reader)
        }
    }
}