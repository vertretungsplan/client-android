/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.TypeConverter
import io.vertretungsplan.client.android.data.util.parseList
import io.vertretungsplan.client.android.data.util.useJsonReader
import io.vertretungsplan.client.android.data.util.useJsonWriter

data class ContentBucketSetItem(
    val id: String,
    val type: ContentBucketSetItemType,
    val passwordParam: String?,
    val usageConditionId: String
) {
    companion object {
        private const val ID = "id"
        private const val TYPE = "type"
        private const val PASSWORD_PARAM = "passwordParam"
        private const val USAGE_CONDITION_ID = "usageConditionId"

        fun parse(reader: JsonReader): ContentBucketSetItem {
            var id: String? = null
            var type: ContentBucketSetItemType? = null
            var passwordParam: String? = null
            var usageConditionId: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextString()
                    TYPE -> type = ContentBucketSetItemTypeConverter.parse(reader.nextString())
                    PASSWORD_PARAM -> passwordParam = reader.nextString()
                    USAGE_CONDITION_ID -> usageConditionId = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ContentBucketSetItem(
                id = id!!,
                type = type!!,
                passwordParam = passwordParam,
                usageConditionId = usageConditionId!!
            )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ID).value(id)
        writer.name(TYPE).value(ContentBucketSetItemTypeConverter.serialize(type))
        writer.name(PASSWORD_PARAM).value(passwordParam)
        writer.name(USAGE_CONDITION_ID).value(usageConditionId)

        writer.endObject()
    }
}

enum class ContentBucketSetItemType {
    Content, Plan
}

object ContentBucketSetItemTypeConverter {
    private const val CONTENT = "content"
    private const val PLAN = "plan"

    fun parse(input: String): ContentBucketSetItemType = when (input) {
        CONTENT -> ContentBucketSetItemType.Content
        PLAN -> ContentBucketSetItemType.Plan
        else -> throw IllegalArgumentException("unexpected content type: $input")
    }

    fun serialize(input: ContentBucketSetItemType): String = when (input) {
        ContentBucketSetItemType.Content -> CONTENT
        ContentBucketSetItemType.Plan -> PLAN
    }
}

class ContentBucketSetItemListConverter {
    @TypeConverter
    fun fromString(value: String): List<ContentBucketSetItem>  = value.useJsonReader { json ->
        json.parseList { ContentBucketSetItem.parse(json) }
    }

    @TypeConverter
    fun serialize(value: List<ContentBucketSetItem>): String = useJsonWriter { json ->
        json.beginArray()
        value.forEach { it.serialize(json) }
        json.endArray()
    }
}