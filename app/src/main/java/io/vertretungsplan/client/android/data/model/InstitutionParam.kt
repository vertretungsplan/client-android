/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(
    tableName = "institution_params",
    primaryKeys = [
        "institution_id",
        "param_id"
    ]
)
data class InstitutionParam(
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "param_id")
    val paramId: String,
    val value: String
)

fun Collection<InstitutionParam>.toMap() = this.associateBy { it.paramId }.mapValues { it.value.value }