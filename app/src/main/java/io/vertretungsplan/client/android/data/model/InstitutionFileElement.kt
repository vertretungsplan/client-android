/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import io.vertretungsplan.client.android.data.HashValidation

@Entity(
    tableName = "institution_file_element",
    primaryKeys = [
        "institution_id",
        "content_bucket_id",
        "file_id",
        "sort"
    ],
    foreignKeys = [
        ForeignKey(
            entity = InstitutionFile::class,
            parentColumns = ["institution_id", "content_bucket_id", "file_id"],
            childColumns = ["institution_id", "content_bucket_id", "file_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class InstitutionFileElement(
    @ColumnInfo(name = "institution_id")
    val institutionId: String,
    @ColumnInfo(name = "content_bucket_id")
    val contentBucketId: String,
    @ColumnInfo(name = "file_id")
    val fileId: String,
    val sort: Int,
    val url: String,
    @ColumnInfo(name = "sha_512")
    val sha512: String,
    val size: Long,
    @ColumnInfo(name = "schedule_background_download")
    val scheduleBackgroundDownload: Boolean
) {
    init {
        HashValidation.assertValidSha512(sha512)
    }
}