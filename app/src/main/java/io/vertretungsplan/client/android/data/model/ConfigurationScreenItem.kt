/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import android.util.JsonReader
import android.util.JsonWriter
import androidx.room.TypeConverter
import io.vertretungsplan.client.android.data.util.parseList
import io.vertretungsplan.client.android.data.util.useJsonReader
import io.vertretungsplan.client.android.data.util.useJsonWriter

data class ConfigurationScreenItem(
    val type: String,
    val param: String,
    val value: String,
    val label: String,
    val visibilityConditionId: String
) {
    companion object {
        private const val TYPE = "type"
        private const val PARAM = "param"
        private const val VALUE = "value"
        private const val LABEL = "label"
        private const val VISIBILITY_CONDITION_ID = "visibilityConditionId"

        fun parse(reader: JsonReader): ConfigurationScreenItem {
            var type: String? = null
            var param: String? = null
            var value: String? = null
            var label: String? = null
            var visibilityConditionId: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    TYPE -> type = reader.nextString()
                    PARAM -> param = reader.nextString()
                    VALUE -> value = reader.nextString()
                    LABEL -> label = reader.nextString()
                    VISIBILITY_CONDITION_ID -> visibilityConditionId = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ConfigurationScreenItem(
                type = type!!,
                param = param!!,
                value = value!!,
                label = label!!,
                visibilityConditionId = visibilityConditionId!!
            )
        }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(TYPE).value(type)
        writer.name(PARAM).value(param)
        writer.name(VALUE).value(value)
        writer.name(LABEL).value(label)
        writer.name(VISIBILITY_CONDITION_ID).value(visibilityConditionId)

        writer.endObject()
    }
}

class ConfigurationScreenItemListConverter {
    @TypeConverter
    fun fromString(value: String): List<ConfigurationScreenItem>  = value.useJsonReader { json ->
        json.parseList { ConfigurationScreenItem.parse(json) }
    }

    @TypeConverter
    fun serialize(value: List<ConfigurationScreenItem>): String = useJsonWriter { json ->
        json.beginArray()
        value.forEach { it.serialize(json) }
        json.endArray()
    }
}