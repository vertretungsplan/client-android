/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.main

import android.content.Context
import android.text.format.DateUtils
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.model.InstitutionFile
import io.vertretungsplan.client.android.data.model.InstitutionMessage
import io.vertretungsplan.client.android.data.model.InstitutionPlanItem
import io.vertretungsplan.client.android.data.model.content.FileContentItemType
import java.text.SimpleDateFormat
import java.util.Locale

sealed class ContentAdapterItem {
    companion object {
        fun formatDate(date: String, context: Context) = try {
            DateUtils.formatDateTime(
                context,
                SimpleDateFormat("yyyy-MM-dd", Locale.US).parse(date)!!.time,
                DateUtils.FORMAT_SHOW_WEEKDAY or DateUtils.FORMAT_SHOW_DATE
            )
        } catch (ex: Exception) {
            date
        }
    }
}
data object ShouldUpdateConfigItem: ContentAdapterItem()
data class ContentAdapterHeader(val titleResourceId: Int): ContentAdapterItem()
data class ContentAdapterFileItem(val file: InstitutionFile): ContentAdapterItem()
data class ContentAdapterDownloadItem(val file: InstitutionFile): ContentAdapterItem()
data class ContentAdapterMessageItem(val message: InstitutionMessage): ContentAdapterItem()
data class ContentAdapterPlanHeader(val date: String, val className: String, val skipDate: Boolean): ContentAdapterItem()
data class ContentAdapterPlanItem(val item: InstitutionPlanItem): ContentAdapterItem()
data object AskForBackgroundSyncItem: ContentAdapterItem()

data class PagerAdapterSection(
    val title: (Context) -> String,
    val items: List<ContentAdapterItem>
)

object ContentAdapterItemGenerator {
    fun buildItems(content: DataMainActivityContent): List<PagerAdapterSection> {
        val result = mutableListOf<PagerAdapterSection>()
        val mainScreenContent = mutableListOf<ContentAdapterItem>()

        result.add(
            PagerAdapterSection(
                title = { it.getString(R.string.content_list_header_overview) },
                items = mainScreenContent
            )
        )

        if (content.institutionInfo.shouldUpdateConfig) {
            mainScreenContent.add(ShouldUpdateConfigItem)
        } else if (content.backgroundSyncEnabled == null) {
            mainScreenContent.add(AskForBackgroundSyncItem)
        }

        if (content.plan.isNotEmpty()) {
            val planByDate = content.plan.groupBy { it.date }
            val planDates = planByDate.keys.sorted()

            planDates.forEach { date ->
                val items = planByDate[date]!!
                val itemsByClass = items
                    .sortedBy { it.lesson }
                    .groupBy { it.className }
                val classNames = itemsByClass.keys

                if (items.size <= 2) {
                    // main screen

                    classNames.forEach { className ->
                        val classItems = itemsByClass[className]!!

                        mainScreenContent.add(
                            ContentAdapterPlanHeader(
                                date = date,
                                className = className,
                                skipDate = false
                            )
                        )

                        classItems.forEach { mainScreenContent.add(ContentAdapterPlanItem(it)) }
                    }
                } else {
                    // extra screen
                    val subscreenContent = mutableListOf<ContentAdapterItem>()

                    classNames.forEach { className ->
                        val classItems = itemsByClass[className]!!

                        subscreenContent.add(
                            ContentAdapterPlanHeader(
                                date = date,
                                className = className,
                                skipDate = true
                            )
                        )

                        classItems.forEach { subscreenContent.add(ContentAdapterPlanItem(it)) }
                    }

                    result.add(
                        PagerAdapterSection(
                            title = { ContentAdapterItem.formatDate(date, it) },
                            items = subscreenContent
                        )
                    )
                }
            }
        }

        val plans = content.institutionFiles.filter { it.type == FileContentItemType.Plan }

        if (plans.isNotEmpty()) {
            mainScreenContent.add(ContentAdapterHeader(R.string.content_list_header_plan))

            plans.forEach { file ->
                mainScreenContent.add(ContentAdapterFileItem(file))
            }
        }

        val messages = content.institutionMessages

        if (messages.isNotEmpty()) {
            mainScreenContent.add(ContentAdapterHeader(R.string.content_list_header_message))

            messages.forEach { message ->
                mainScreenContent.add(ContentAdapterMessageItem(message))
            }
        }

        val downloads = content.institutionFiles.filter { it.type == FileContentItemType.Download }

        if (downloads.isNotEmpty()) {
            val downloadItems = downloads.map { file -> ContentAdapterDownloadItem(file) }

            if (downloads.size <= 6) {
                mainScreenContent.add(ContentAdapterHeader(R.string.content_list_header_download))
                mainScreenContent.addAll(downloadItems)
            } else {
                result.add(
                    PagerAdapterSection(
                        title = { it.getString(R.string.content_list_header_download) },
                        items = downloadItems
                    )
                )
            }
        }

        // remove the overview screen if it is empty and there are other screens
        if (result.first().items.isEmpty() && result.size > 1) {
            result.removeAt(0)
        }

        return result
    }
}