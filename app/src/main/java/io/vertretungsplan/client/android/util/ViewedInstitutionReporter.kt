/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.util

import android.util.Log
import androidx.lifecycle.LiveData
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.registry.AppRegistry
import java.util.*

class ViewedInstitutionReporter (private val registry: AppRegistry) {
    companion object {
        private const val LOG_TAG = "ViewedInstitutionReport"
    }

    private val countersByInstitutionId = Collections.synchronizedMap(mutableMapOf<String, Int>())

    fun reportStart(institutionId: String) {
        val current = countersByInstitutionId[institutionId] ?: 0

        countersByInstitutionId[institutionId] = current + 1

        if (current == 0) {
            onInstitutionStarted(institutionId)
        }
    }

    fun reportStop(institutionId: String) {
        val current = countersByInstitutionId[institutionId] ?: 0

        if (current == 0) {
            throw IllegalStateException()
        }

        if (current == 1) {
            onInstitutionStopped(institutionId)
        }

        countersByInstitutionId[institutionId] = current - 1
    }

    fun isInstitutionStarted(institutionId: String) = (countersByInstitutionId[institutionId]?: 0) > 0

    private fun onInstitutionStarted(institutionId: String) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "onInstitutionStarted($institutionId)")
        }

        registry.notifyInstitution.dismissNotificationByInstitutionIdAsync(institutionId)
    }

    private fun onInstitutionStopped(institutionId: String) {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "onInstitutionStopped($institutionId)")
        }
    }
}

fun ViewedInstitutionReporter.createReportObservable(institutionId: String) = object: LiveData<Unit?>() {
    override fun onActive() {
        super.onActive()

        reportStart(institutionId)
        postValue(null)
    }

    override fun onInactive() {
        super.onInactive()

        reportStop(institutionId)
    }
}