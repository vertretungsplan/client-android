/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model.content

import android.util.JsonReader
import io.vertretungsplan.client.android.data.util.parseList

data class InstitutionContentBucket(
    val file: List<FileContentItem>,
    val message: List<MessageContentItem>
) {
    companion object {
        fun parse(reader: JsonReader): InstitutionContentBucket {
            var file: List<FileContentItem>? = null
            var message: List<MessageContentItem>? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "file" -> file = reader.parseList { FileContentItem.parse(reader) }
                    "message" -> message = reader.parseList { MessageContentItem.parse(reader) }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return InstitutionContentBucket(
                file = file!!,
                message = message!!
            )
        }
    }
}