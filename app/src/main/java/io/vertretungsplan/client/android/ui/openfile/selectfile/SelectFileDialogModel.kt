/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.selectfile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import io.vertretungsplan.client.android.data.model.InstitutionFile
import io.vertretungsplan.client.android.data.model.InstitutionFileElement
import io.vertretungsplan.client.android.registry.AppRegistry

class SelectFileDialogModel: ViewModel() {
    private var hadInit = false
    lateinit var content: LiveData<Pair<InstitutionFile, List<InstitutionFileElement>>?>

    fun init(
        institutionId: String, bucketId: String, fileId: String,
        registry: AppRegistry
    ) {
        if (hadInit) {
            return
        }

        hadInit = true

        val fileLive = registry.database.institutionFileDao().getFileByInstitutionAndIdLive(
            institutionId = institutionId,
            bucketId = bucketId,
            fileId = fileId
        )

        val fileElementsLive = registry.database.institutionFileElementDao().getByInstitutionAndIdLive(
            institutionId = institutionId,
            contentBucketId = bucketId,
            fileId = fileId
        )

        content = fileLive.switchMap { file ->
            fileElementsLive.map { fileElements ->
                if (file == null) {
                    null
                } else {
                    file to fileElements
                }
            }
        }
    }
}