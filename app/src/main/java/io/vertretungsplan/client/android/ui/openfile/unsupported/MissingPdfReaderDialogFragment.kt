/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.unsupported

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import io.vertretungsplan.client.android.R

class MissingPdfReaderDialogFragment: DialogFragment() {
    companion object {
        private const val DIALOG_TAG = "MissingPdfReaderDialogFragment"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = AlertDialog.Builder(context!!, theme)
        .setTitle(R.string.openfile_missingpdfreader_title)
        .setMessage(R.string.openfile_missingpdfreader_text)
        .setPositiveButton(R.string.openfile_missingpdfreader_action) { _, _ ->
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=com.gsnathan.pdfviewer")
                    )
                )
            } catch (ex: Exception) {
                Toast.makeText(context!!, R.string.generic_failed_toast, Toast.LENGTH_SHORT).show()
            }
        }
        .setNegativeButton(R.string.generic_cancel, null)
        .create()

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}