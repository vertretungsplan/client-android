/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data

object HashValidation {
    private val sha512Regex = Regex("^[0-9a-f]{128}$")

    fun isValidSha512(hash: String) = sha512Regex.matches(hash)

    fun assertValidSha512(hash: String) {
        if (!isValidSha512(hash)) {
            throw RuntimeException("invalid hash")
        }
    }
}