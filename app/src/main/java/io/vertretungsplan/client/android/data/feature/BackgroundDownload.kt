/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.feature

import android.util.Log
import androidx.work.WorkManager
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.model.InstitutionFileElement
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.worker.BackgroundDownloadWorker
import io.vertretungsplan.client.android.worker.BackgroundSyncWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.channels.receiveOrNull
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BackgroundDownload (private val appRegistry: AppRegistry) {
    companion object {
        private const val LOG_TAG = "BackgroundDownload"
    }

    suspend fun eventuallyEnqueue() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "eventuallyEnqueue()")
        }

        val pendingDownloads = appRegistry.database.institutionFileElementDao().getPendingDownloads()

        if (pendingDownloads.isEmpty()) {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "nothing pending => don't enqueue")
            }
        } else {
            if (BuildConfig.DEBUG) {
                Log.d(LOG_TAG, "something pending => enqueue")
            }

            BackgroundDownloadWorker.enqueue(appRegistry.workManager)
        }
    }

    suspend fun doBackgroundDownload() {
        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "started")
        }

        val pendingDownloads = appRegistry.database.institutionFileElementDao().getPendingDownloads()

        if (pendingDownloads.isNotEmpty()) {
            coroutineScope {
                val channel = Channel<InstitutionFileElement>(Channel.RENDEZVOUS)

                repeat(2) {
                    launch {
                        while (true) {
                            val download = channel.receiveCatching().getOrNull() ?: break

                            if (BuildConfig.DEBUG) {
                                Log.d(LOG_TAG, "downloading ${download.url}")
                            }

                            appRegistry.downloadFile.doDownloadAsync(
                                sha512 = download.sha512,
                                url = download.url,
                                size = download.size
                            ).let { channel ->
                                channel.consume {
                                    for (downloadStatus in channel) {
                                        if (downloadStatus is DownloadFileFailed) {
                                            if (BuildConfig.DEBUG) {
                                                Log.d(
                                                    LOG_TAG,
                                                    "failed downloading ${download.url}",
                                                    downloadStatus.error
                                                )
                                            }

                                            break
                                        } else if (downloadStatus is DownloadFileStatusDone) {
                                            if (BuildConfig.DEBUG) {
                                                Log.d(LOG_TAG, "done downloading ${download.url}")
                                            }

                                            break
                                        } else {
                                            // continue waiting
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                pendingDownloads.forEach { channel.send(it) }
                channel.close()
            }
        }

        if (BuildConfig.DEBUG) {
            Log.d(LOG_TAG, "finished")
        }
    }

    suspend fun descheduleDownload(sha512: String) {
        withContext(Dispatchers.IO) {
            appRegistry.database.institutionFileElementDao().removeDownloadPendingSync(sha512)
        }
    }
}