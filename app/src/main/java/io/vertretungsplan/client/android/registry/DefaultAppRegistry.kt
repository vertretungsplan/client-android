/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.registry

import android.content.Context
import androidx.work.WorkManager
import io.vertretungsplan.client.android.data.AppDatabase
import io.vertretungsplan.client.android.data.cas.CasStorage
import io.vertretungsplan.client.android.data.client.generic.ApiClientCreator
import io.vertretungsplan.client.android.data.client.http.HttpApiClientCreator
import io.vertretungsplan.client.android.data.client.http.HttpClient
import io.vertretungsplan.client.android.data.feature.NotifyInstitution
import io.vertretungsplan.client.android.util.SingletonCreator
import okhttp3.OkHttpClient
import java.io.File

class DefaultAppRegistry(private val context: Context): AppRegistry() {
    companion object {
        private val instanceCreator = SingletonCreator.singleton { DefaultAppRegistry(it) }

        fun with(context: Context) = instanceCreator(context)
    }

    override val cas: CasStorage by lazy { CasStorage(File(context.cacheDir, "cas")) }
    override val database: AppDatabase by lazy { AppDatabase.createDiskBuilder("vp_data", context).build() }
    override val httpClient: OkHttpClient = HttpClient.instance
    override val apiClientCreator: ApiClientCreator by lazy { HttpApiClientCreator(httpClient) }
    override val notifyInstitution: NotifyInstitution by lazy { NotifyInstitution(this, context) }
    override val workManager: WorkManager
        get() = WorkManager.getInstance(context)
}