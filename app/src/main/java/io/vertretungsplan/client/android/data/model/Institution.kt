/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Institution (
    @PrimaryKey
    val id: String,
    val title: String,
    @ColumnInfo(name = "content_server_url")
    val contentServerUrl: String?,
    @ColumnInfo(name = "should_update_config")
    val shouldUpdateConfig: Boolean
) {
    companion object {
        fun parse(reader: JsonReader): Institution {
            var id: String? = null
            var title: String? = null
            var contentServerUrl: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    "id" -> id = reader.nextString()
                    "title" -> title = reader.nextString()
                    "contentServerUrl" -> contentServerUrl = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return Institution(
                id = id!!,
                title = title!!,
                contentServerUrl = contentServerUrl,
                shouldUpdateConfig = false
            )
        }
    }
}