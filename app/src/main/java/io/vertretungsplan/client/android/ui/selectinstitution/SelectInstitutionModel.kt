/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.selectinstitution

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.data.dao.getValueSync
import io.vertretungsplan.client.android.data.dao.setValueSync
import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.data.model.config.ConfigItemKey
import io.vertretungsplan.client.android.registry.AppRegistry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class SelectInstitutionModel: ViewModel() {
    companion object {
        private const val LOG_TAG = "SelectInstitutionModel"
    }

    enum class Mode {
        Loading,
        NeedsListServer,
        Ready,
        ReadyWithFailedUpdate
    }

    private var hadInit = false
    private val statusInternal = MutableLiveData<Mode>().apply { value = Mode.Loading }

    private lateinit var registry: AppRegistry

    lateinit var listContent: LiveData<List<Institution>>
    val status: LiveData<Mode> = statusInternal

    fun init(registry: AppRegistry) {
        if (hadInit) {
            return
        }

        hadInit = true

        this.registry = registry
        this.listContent = registry.database.institution().getListLive().map { list -> list.sortedBy { it.title.toLowerCase(Locale.US) } }

        updateList()
    }

    fun updateList() {
        statusInternal.value = Mode.Loading

        GlobalScope.launch (Dispatchers.IO) {
            val serverUrl = registry.database.config().getValueSync(ConfigItemKey.LIST_SERVER_URL)

            if (serverUrl == null) {
                statusInternal.postValue(Mode.NeedsListServer)

                return@launch
            }

            try {
                registry.syncInstitutionList.forceSync()

                statusInternal.postValue(Mode.Ready)
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "could not sync institution list", ex)
                }

                statusInternal.postValue(Mode.ReadyWithFailedUpdate)
            }
        }
    }

    fun setSelectionSync(institutionId: String) {
        registry.database.config().setValueSync(ConfigItemKey.CURRENT_INSTITUTION_ID, institutionId)
    }
}

