/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.vertretungsplan.client.android.data.model.InstitutionParam

@Dao
interface InstitutionParamDao {
    @Query("DELETE FROM institution_params WHERE institution_id = :institutionId")
    fun removeByInstitutionId(institutionId: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(items: List<InstitutionParam>)

    @Query("SELECT * FROM institution_params WHERE institution_id = :institutionId")
    suspend fun queryByInstitutionIdCoroutine(institutionId: String): List<InstitutionParam>
}