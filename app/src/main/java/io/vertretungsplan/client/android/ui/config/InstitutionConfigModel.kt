/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.config

import android.app.Application
import android.util.Log
import androidx.compose.material3.SnackbarHostState
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.data.model.InstitutionConfigBucket
import io.vertretungsplan.client.android.data.model.InstitutionParam
import io.vertretungsplan.client.android.data.model.toMap
import io.vertretungsplan.client.android.data.util.ConditionSet
import io.vertretungsplan.client.android.data.util.ConditionSetEvaluationException
import io.vertretungsplan.client.android.registry.AppRegistry
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

class InstitutionConfigModel(application: Application): AndroidViewModel(application) {
    companion object {
        private const val LOG_TAG = "InstitutionConfigModel"

        fun buildOptions(
            configBuckets: Map<String, InstitutionConfigBucket>,
            params: Map<String, String>,
            invalidPasswords: Set<String>
        ): List<InstitutionConfigScreenItem> {
            val result = mutableListOf<InstitutionConfigScreenItem>()
            val handledBuckets = mutableSetOf<String>()

            fun handleBucket(bucketId: String) {
                val listItemIdBase = bucketId.hashCode().toLong() shl 32

                if (!handledBuckets.add(bucketId)) {
                    result.add(MissingOptionsItem(
                        id = listItemIdBase,
                        textResource = R.string.config_item_missing_bucket
                    ))

                    return
                }

                val bucket = configBuckets[bucketId] ?: return

                bucket.config.forEachIndexed { index, configItem ->
                    val id = listItemIdBase + 1 + index

                    val isVisible = try {
                        ConditionSet.evaluate(
                            params = params,
                            conditionId = configItem.visibilityConditionId,
                            conditionSet = bucket.conditions
                        )
                    } catch (ex: ConditionSetEvaluationException) {
                        result.add(MissingOptionsItem(
                            id = id,
                            textResource = R.string.config_item_invalid_visibility
                        ))

                        return@forEachIndexed
                    }

                    if (isVisible) {
                        if (configItem.type == "otherConfigBucket") {
                            handleBucket(configItem.value)
                        } else {
                            val option = InstitutionConfigScreenItem.parse(
                                id = id,
                                item = configItem,
                                params = params,
                                invalidPasswords = invalidPasswords
                            ) ?: MissingOptionsItem(id = id, textResource = R.string.config_item_unsupported_type)

                            result.add(option)
                        }
                    }
                }
            }

            handleBucket("default")

            if (result.isEmpty()) {
                result.add(
                    MissingOptionsItem(
                        id = 0,
                        textResource = R.string.config_item_no_options
                    )
                )
            }

            return result.toList()
        }
    }

    sealed class ActivityCommand {
        data class Finish(val ok: Boolean = false): ActivityCommand()
    }

    private var hadInit = false
    lateinit var institutionId: String
    lateinit var registry: AppRegistry
    lateinit var institution: LiveData<Institution?>

    private var snackbarJob: Job? = null
    val snackbar = SnackbarHostState()

    private val configBuckets = MutableLiveData<Map<String, InstitutionConfigBucket>>().apply { value = emptyMap() }
    private val params = MutableLiveData<Map<String, String>>().apply { value = emptyMap() }
    private val invalidPasswords = MutableLiveData<Set<String>>().apply { value = emptySet() }

    val commandChannel = Channel<ActivityCommand>(Channel.RENDEZVOUS)
    val isBusy = MutableLiveData<Boolean>()

    val optionsLive = configBuckets.switchMap { buckets ->
        params.switchMap { params ->
            invalidPasswords.map { invalidPasswords ->
                buildOptions(
                    configBuckets = buckets,
                    params = params,
                    invalidPasswords = invalidPasswords
                )
            }
        }
    }

    fun init(institutionId: String, registry: AppRegistry) {
        if (hadInit) {
            return
        }

        hadInit = true

        this.institutionId = institutionId
        this.registry = registry

        institution = registry.database.institution().getByIdLive(institutionId)

        isBusy.value = true

        GlobalScope.launch (Dispatchers.Main) {
            params.value = registry.database.institutionParam().queryByInstitutionIdCoroutine(institutionId).toMap()

            sync()
        }
    }

    fun sync() {
        val doneOnSuccess = configBuckets.value!!.isNotEmpty()

        isBusy.value = true
        snackbarJob?.cancel()

        GlobalScope.launch (Dispatchers.Main) {
            try {
                val content = coroutineScope {
                    launch { delay(500) }

                    val institutionEntry = registry.database.institution().getByIdCoroutine(institutionId)
                    val client = registry.syncInstitution.createApiClient(institutionEntry!!)

                    registry.syncInstitution.query(
                        institutionId = institutionId,
                        client = client,
                        params = params.value!!
                    )
                }

                // update params to set that password is invalid
                invalidPasswords.value = content.invalidPasswordParams
                configBuckets.value = content.configBuckets

                if (doneOnSuccess) {
                    if (content.configValidationFailed) {
                        snackbarJob = launch {
                            snackbar.showSnackbar(
                                getApplication<Application>().getString(R.string.config_snackbar_incomplete)
                            )
                        }
                    } else if (content.invalidPasswordParams.isNotEmpty()) {
                        snackbarJob = launch {
                            snackbar.showSnackbar(
                                getApplication<Application>().getString(R.string.config_snackbar_invalid_password)
                            )
                        }
                    } else {
                        withContext(Dispatchers.IO) { saveParamsSync() }

                        commandChannel.send(ActivityCommand.Finish(ok = true))

                        // don't hide progress screen for one short moment
                        return@launch
                    }
                }

                isBusy.value = false
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.w(LOG_TAG, "sync() failed", ex)
                }

                snackbarJob = launch {
                    if (doneOnSuccess) {
                        snackbar.showSnackbar(
                            getApplication<Application>().getString(R.string.config_snackbar_validation_failed)
                        )
                    } else {
                        snackbar.showSnackbar(
                            getApplication<Application>().getString(R.string.config_snackbar_loading_failed)
                        )

                        commandChannel.send(ActivityCommand.Finish())
                    }
                }

                isBusy.value = false
            }
        }
    }

    fun updateParam(key: String, value: String) {
        params.value = params.value!! + mapOf(key to value)
    }

    private fun saveParamsSync() {
        registry.database.runInTransaction {
            registry.database.institutionParam().removeByInstitutionId(institutionId)
            registry.database.institutionParam().insert(
                params.value!!.entries.map { entry ->
                    InstitutionParam(
                        institutionId = institutionId,
                        paramId = entry.key,
                        value = entry.value
                    )
                }
            )
        }
    }
}