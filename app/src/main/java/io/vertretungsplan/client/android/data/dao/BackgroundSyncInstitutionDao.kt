/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.vertretungsplan.client.android.data.model.BackgroundSyncInstitution

@Dao
interface BackgroundSyncInstitutionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(item: BackgroundSyncInstitution)

    @Query("SELECT * FROM background_sync_institution WHERE enable_background_sync = 1")
    suspend fun getInstitutionsWithBackgroundSync(): List<BackgroundSyncInstitution>

    @Query("SELECT * FROM background_sync_institution WHERE institution_id = :institutionId")
    fun getInstitutionBackgroundSyncConfig(institutionId: String): LiveData<BackgroundSyncInstitution?>
}