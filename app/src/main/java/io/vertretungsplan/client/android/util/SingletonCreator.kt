/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.util

import android.content.Context
import android.os.Handler
import android.os.Looper
import java.util.concurrent.CountDownLatch

object SingletonCreator {
    private val handler = Handler(Looper.getMainLooper())

    fun <T> singleton(creator: (Context) -> T): (Context) -> T {
        var instance: T? = null

        fun createInstanceIfItDoesNotExistAlready(applicationContext: Context) {
            if (instance == null) {
                instance = creator(applicationContext)
            }
        }

        return {
            context ->

            val applicationContext = context.applicationContext

            if (instance == null) {
                if (Looper.getMainLooper() == Looper.myLooper()) {
                    // at the main Thread: just create it

                    createInstanceIfItDoesNotExistAlready(applicationContext)
                } else {
                    // at a background thread: schedule creation at the main Thread and wait

                    val latch = CountDownLatch(1)

                    handler.post {
                        createInstanceIfItDoesNotExistAlready(applicationContext)
                        latch.countDown()
                    }

                    latch.await()
                }
            }

            instance!!
        }
    }
}
