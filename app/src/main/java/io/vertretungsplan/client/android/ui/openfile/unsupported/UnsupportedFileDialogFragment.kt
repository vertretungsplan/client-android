/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.unsupported

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import io.vertretungsplan.client.android.R

class UnsupportedFileDialogFragment: DialogFragment() {
    companion object {
        private const val DIALOG_TAG = "UnsupportedFileDialogFragment"
        private const val MIME_TYPE = "mimeType"

        fun newInstance(mimeType: String) = UnsupportedFileDialogFragment().apply {
            arguments = Bundle().apply {
                putString(MIME_TYPE, mimeType)
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val mimeType = arguments!!.getString(MIME_TYPE)

        return AlertDialog.Builder(context!!, theme)
            .setTitle(R.string.openfile_unsupported_title)
            .setMessage(getString(R.string.openfile_unsupported_text, mimeType))
            .setPositiveButton(R.string.generic_ok, null)
            .create()
    }

    fun show(fragmentManager: FragmentManager) = show(fragmentManager, DIALOG_TAG)
}