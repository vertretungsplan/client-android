/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.vertretungsplan.client.android.data.model.InstitutionFileElement

@Dao
interface InstitutionFileElementDao {
    @Insert
    fun insert(items: List<InstitutionFileElement>)

    @Query("DELETE FROM institution_file_element WHERE institution_id = :institutionId AND content_bucket_id = :contentBucketId AND file_id = :fileId")
    fun removeByFileId(institutionId: String, contentBucketId: String, fileId: String)

    @Query("SELECT * FROM institution_file_element WHERE institution_id = :institutionId AND content_bucket_id = :contentBucketId AND file_id = :fileId ORDER BY sort ASC")
    fun getByFileIdSync(institutionId: String, contentBucketId: String, fileId: String): List<InstitutionFileElement>

    @Query("SELECT * FROM institution_file_element WHERE institution_id = :institutionId AND content_bucket_id = :contentBucketId AND file_id = :fileId ORDER BY sort ASC")
    fun getByInstitutionAndIdLive(institutionId: String, contentBucketId: String, fileId: String): LiveData<List<InstitutionFileElement>>

    @Query("SELECT * FROM institution_file_element WHERE schedule_background_download = 1")
    suspend fun getPendingDownloads(): List<InstitutionFileElement>

    @Query("UPDATE institution_file_element SET schedule_background_download = 0 WHERE sha_512 = :sha512")
    fun removeDownloadPendingSync(sha512: String)

    @Query("SELECT COUNT(sha_512) FROM institution_file_element WHERE sha_512 = :hash")
    suspend fun countHashCoroutine(hash: String): Long
}