/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.util

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class TaskDeduplication<Request, Response>(private val handler: suspend (Request) -> Response) {
    private val lock = Mutex()
    private val tasks = mutableMapOf<Request, Deferred<Response>>()

    suspend fun handle(request: Request): Response {
        return lock.withLock {
            val oldTask = tasks[request]

            if (oldTask != null) {
                oldTask
            } else {
                val newTask = GlobalScope.async {
                    try {
                        handler(request)
                    } finally {
                        lock.withLock {
                            tasks.remove(request)
                        }
                    }
                }

                tasks[request] = newTask

                newTask
            }
        }.await()
    }
}