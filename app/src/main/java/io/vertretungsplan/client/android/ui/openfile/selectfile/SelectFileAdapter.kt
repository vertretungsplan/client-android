/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.openfile.selectfile

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.vertretungsplan.client.android.data.model.InstitutionFileElement
import kotlin.properties.Delegates

class SelectFileAdapter: RecyclerView.Adapter<SelectFileHolder>() {
    var items: List<InstitutionFileElement> by Delegates.observable(emptyList()) { _, _, _ -> notifyDataSetChanged() }
    var listener: SelectFileAdapterListener? = null

    init {
        setHasStableIds(true)
    }

    override fun getItemCount() = items.size

    override fun getItemId(position: Int): Long = items[position].sha512.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SelectFileHolder(
        LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false) as TextView
    )

    override fun onBindViewHolder(holder: SelectFileHolder, position: Int) {
        val item = items[position]

        holder.textView.text = (position + 1).toString()
        holder.textView.setOnClickListener { listener?.onFileClicked(item) }
    }
}

class SelectFileHolder(val textView: TextView): RecyclerView.ViewHolder(textView)

interface SelectFileAdapterListener {
    fun onFileClicked(fileElement: InstitutionFileElement)
}