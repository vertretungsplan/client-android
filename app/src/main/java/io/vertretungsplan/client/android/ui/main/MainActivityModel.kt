/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.ui.main

import android.app.Application
import android.util.Log
import androidx.compose.material3.SnackbarHostState
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import io.vertretungsplan.client.android.BuildConfig
import io.vertretungsplan.client.android.R
import io.vertretungsplan.client.android.data.dao.getValueCoroutine
import io.vertretungsplan.client.android.data.dao.getValueLive
import io.vertretungsplan.client.android.data.dao.setValueSync
import io.vertretungsplan.client.android.data.model.*
import io.vertretungsplan.client.android.data.model.config.ConfigItemKey
import io.vertretungsplan.client.android.registry.AppRegistry
import io.vertretungsplan.client.android.util.createReportObservable
import kotlinx.coroutines.*

class MainActivityModel(application: Application): AndroidViewModel(application) {
    private var didInit = false
    private lateinit var registry: AppRegistry
    private val selectedInstitutionIdBase = MutableLiveData<LiveData<String?>>()
    private val selectedInstitutionId = selectedInstitutionIdBase.switchMap { idLive ->
        idLive.switchMap { id ->
            if (id != null) {
                registry.viewedInstitutionReporter.createReportObservable(id).map { id }
            } else {
                MutableLiveData<String?>().apply { value = id }
            }
        }
    }

    lateinit var content: LiveData<MainActivityContent>

    private var ignoreStartOnce = false

    private var lastRefreshStatusMessage: Job? = null

    val snackbarHostState = SnackbarHostState()

    fun init(registry: AppRegistry, launchInstitutionId: String?) {
        if (didInit) {
            return
        }

        didInit = true

        this.registry = registry

        if (launchInstitutionId == null) {
            selectedInstitutionIdBase.value = registry.database.config().getValueLive(ConfigItemKey.CURRENT_INSTITUTION_ID)
        } else {
            GlobalScope.launch (Dispatchers.Main) {
                withContext(Dispatchers.IO) {
                    registry.database.config().setValueSync(ConfigItemKey.CURRENT_INSTITUTION_ID, launchInstitutionId)
                }

                selectedInstitutionIdBase.value = registry.database.config().getValueLive(ConfigItemKey.CURRENT_INSTITUTION_ID)
            }
        }

        content = selectedInstitutionId.switchMap { institutionId ->
            if (institutionId == null) {
                return@switchMap MissingInstitutionMainActivityContent.live
            }

            val backgroundSyncLive = registry.database.backgroundSyncInstitution().getInstitutionBackgroundSyncConfig(institutionId)
            val institutionInfoLive = registry.database.institution().getByIdLive(institutionId)
            val institutionFilesLive = registry.database.institutionFileDao().getFilesByInstitutionLive(institutionId)
            val institutionMessagesLive = registry.database.institutionMessageDao().getMessagesByInstitutionLive(institutionId)
            val institutionPlanLive = registry.database.institutionPlanItemDao().getPlanItemsByInstitutionLive(institutionId)

            backgroundSyncLive.switchMap { backgroundSync ->
                institutionInfoLive.switchMap { institutionInfo ->
                    if (institutionInfo == null) {
                        MissingInstitutionMainActivityContent.live
                    } else {
                        institutionFilesLive.switchMap { institutionFiles ->
                            institutionMessagesLive.switchMap { institutionMessages ->
                                institutionPlanLive.map { institutionPlan ->
                                    DataMainActivityContent(
                                        institutionInfo = institutionInfo,
                                        institutionFiles = institutionFiles,
                                        institutionMessages = institutionMessages,
                                        plan = institutionPlan,
                                        backgroundSyncEnabled = backgroundSync?.enableBackgroundSync
                                    ) as MainActivityContent
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun refresh() {
        GlobalScope.launch (Dispatchers.Main) {
            selectedInstitutionIdBase.value = registry.database.config().getValueLive(ConfigItemKey.CURRENT_INSTITUTION_ID)

            val institutionId = registry.database.config().getValueCoroutine(ConfigItemKey.CURRENT_INSTITUTION_ID) ?: return@launch
            val institution = registry.database.institution().getByIdCoroutine(institutionId) ?: return@launch

            showRefreshStatusMessage(
                title = institution.title,
                status = RefreshStatusMessageType.Refreshing
            )

            try {
                coroutineScope {
                    launch { delay(100) }

                    registry.syncInstitution.sync(institutionId)
                }

                showRefreshStatusMessage(
                    title = institution.title,
                    status = RefreshStatusMessageType.Done
                )
            } catch (ex: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.w("MainActivity", "sync failed", ex)
                }

                showRefreshStatusMessage(
                    title = institution.title,
                    status = RefreshStatusMessageType.Failed
                )
            }
        }
    }

    fun setBackgroundSyncEnabled(enable: Boolean) {
        GlobalScope.launch (Dispatchers.Main) {
            val institutionId = registry.database.config().getValueCoroutine(ConfigItemKey.CURRENT_INSTITUTION_ID) ?: return@launch

            GlobalScope.launch (Dispatchers.IO) {
                registry.database.backgroundSyncInstitution().upsert(
                    BackgroundSyncInstitution(
                        institutionId = institutionId,
                        enableBackgroundSync = enable
                    )
                )
            }
        }
    }

    private fun showRefreshStatusMessage(
        title: String,
        status: RefreshStatusMessageType
    ) {
        lastRefreshStatusMessage?.cancel()

        lastRefreshStatusMessage = viewModelScope.launch {
            snackbarHostState.showSnackbar(
                getApplication<Application>().getString(
                    when (status) {
                        RefreshStatusMessageType.Refreshing -> R.string.main_status_refreshing
                        RefreshStatusMessageType.Failed -> R.string.main_status_failed
                        RefreshStatusMessageType.Done -> R.string.main_status_done
                    },
                    title
                )
            )
        }
    }

    fun reportOnStart() {
        if (ignoreStartOnce) {
            ignoreStartOnce = false
        } else {
            refresh()
        }

        registry.syncInstitutionList.syncIfLastSyncLongAgoAsync()
    }

    fun ignoreOneStart() {
        ignoreStartOnce = true
    }
}

sealed class MainActivityContent

object MissingInstitutionMainActivityContent: MainActivityContent() {
    val live = MutableLiveData<MainActivityContent>().apply { postValue(MissingInstitutionMainActivityContent) }
}

data class DataMainActivityContent(
    val institutionInfo: Institution,
    val institutionFiles: List<InstitutionFile>,
    val institutionMessages: List<InstitutionMessage>,
    val plan: List<InstitutionPlanItem>,
    val backgroundSyncEnabled: Boolean?
): MainActivityContent()

enum class RefreshStatusMessageType {
    Refreshing,
    Done,
    Failed
}