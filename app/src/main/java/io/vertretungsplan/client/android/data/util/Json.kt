/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.util

import android.util.JsonReader
import android.util.JsonToken
import android.util.JsonWriter
import java.io.StringReader
import java.io.StringWriter

inline fun <R> String.useJsonReader(block: (JsonReader) -> R): R {
    StringReader(this).use { reader ->
        return JsonReader(reader).use(block)
    }
}

fun useJsonWriter(block: (JsonWriter) -> Unit): String {
    StringWriter().use { writer ->
        JsonWriter(writer).use { json ->
            block(json)
        }

        return writer.buffer.toString()
    }
}

fun <T> JsonReader.parseList(parseItem: () -> T): List<T> {
    val result = mutableListOf<T>()

    this.beginArray()
    while (this.hasNext()) {
        result.add(parseItem())
    }
    this.endArray()

    return result.toList()
}

fun <T> JsonReader.parseStringMap(parseItem: () -> T): Map<String, T> {
    val result = mutableMapOf<String, T>()

    this.beginObject()
    while (this.hasNext()) {
        val key = this.nextName()
        val value = parseItem()

        result[key] = value
    }
    this.endObject()

    return result.toMap()
}

fun JsonReader.nextStringOrNull(): String? = if (this.peek() == JsonToken.NULL) {
    this.nextNull()

    null
} else {
    this.nextString()
}
