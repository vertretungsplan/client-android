/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.registry

import androidx.work.WorkManager
import io.vertretungsplan.client.android.data.AppDatabase
import io.vertretungsplan.client.android.data.cas.CasStorage
import io.vertretungsplan.client.android.data.client.generic.ApiClientCreator
import io.vertretungsplan.client.android.data.feature.*
import io.vertretungsplan.client.android.util.ViewedInstitutionReporter
import okhttp3.OkHttpClient

abstract class AppRegistry {
    abstract val cas: CasStorage
    abstract val database: AppDatabase
    abstract val httpClient: OkHttpClient
    abstract val apiClientCreator: ApiClientCreator
    abstract val notifyInstitution: NotifyInstitution
    abstract val workManager: WorkManager

    open val clearStorage: ClearStorage by lazy { ClearStorage(cas, database) }
    open val downloadFile: DownloadFile by lazy { DownloadFile(this) }
    open val backgroundSync: BackgroundSync by lazy { BackgroundSync(this) }
    open val syncInstitution: SyncInstitution by lazy { SyncInstitution(this) }
    open val syncInstitutionList: SyncInstitutionList by lazy { SyncInstitutionList(this) }
    open val viewedInstitutionReporter: ViewedInstitutionReporter by lazy { ViewedInstitutionReporter(this) }
    open val backgroundDownload: BackgroundDownload by lazy { BackgroundDownload(this) }
}