/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.client.generic

import io.vertretungsplan.client.android.data.model.Institution
import io.vertretungsplan.client.android.data.model.InstitutionConfigBucket
import io.vertretungsplan.client.android.data.model.content.InstitutionContentBucket
import io.vertretungsplan.client.android.data.model.content.PlanContent

interface ApiClient {
    fun queryInstitutionList(): List<Institution>
    fun queryConfigBucket(institutionId: String, bucketId: String, password: String?): InstitutionConfigBucket
    fun queryContentBucket(institutionId: String, bucketId: String, password: String?): InstitutionContentBucket
    fun queryPlan(institutionId: String, bucketId: String, password: String?): PlanContent
}