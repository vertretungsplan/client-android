/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.vertretungsplan.client.android.data.dao.*
import io.vertretungsplan.client.android.data.model.*
import io.vertretungsplan.client.android.data.model.config.ConfigItem

@Database(
    version = 1,
    entities = [
        BackgroundSyncInstitution::class,
        ConfigItem::class,
        Institution::class,
        InstitutionConfigBucket::class,
        InstitutionFile::class,
        InstitutionFileElement::class,
        InstitutionMessage::class,
        InstitutionParam::class,
        InstitutionPlanItem::class
    ]
)
abstract class AppDatabase: RoomDatabase() {
    companion object {
        fun createDiskBuilder(filename: String, context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, filename)
            .addMigrations(
                // nothing yet
            )
    }

    abstract fun backgroundSyncInstitution(): BackgroundSyncInstitutionDao
    abstract fun config(): ConfigDao
    abstract fun institution(): InstitutionDao
    abstract fun institutionConfigDao(): InstitutionConfigDao
    abstract fun institutionFileDao(): InstitutionFileDao
    abstract fun institutionFileElementDao(): InstitutionFileElementDao
    abstract fun institutionMessageDao(): InstitutionMessageDao
    abstract fun institutionParam(): InstitutionParamDao
    abstract fun institutionPlanItemDao(): InstitutionPlanItemDao
}