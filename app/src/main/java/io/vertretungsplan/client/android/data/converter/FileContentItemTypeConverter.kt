/**
 * vertretungsplan.io android client
 *
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see https://www.gnu.org/licenses/.
 */
package io.vertretungsplan.client.android.data.converter

import androidx.room.TypeConverter
import io.vertretungsplan.client.android.data.model.content.FileContentItemType

class FileContentItemTypeConverter {
    companion object {
        private const val PLAN = "plan"
        private const val DOWNLOAD = "download"
    }

    @TypeConverter
    fun fromString(value: String): FileContentItemType = when (value) {
        PLAN -> FileContentItemType.Plan
        DOWNLOAD -> FileContentItemType.Download
        else -> throw IllegalArgumentException()
    }

    @TypeConverter
    fun toString(value: FileContentItemType) = when (value) {
        FileContentItemType.Plan -> PLAN
        FileContentItemType.Download -> DOWNLOAD
    }
}