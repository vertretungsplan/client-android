## Android client

This is the Android Client for vertretungsplan.io

### Screenshots

![Screenshot which shows the content of a school whose plan is included as list of files](./app/src/main/play/listings/en-US/graphics/phone-screenshots/1.png)

![Screenshot which shows the content of a school whose plan is parsed](./app/src/main/play/listings/en-US/graphics/phone-screenshots/2.png)

### Overview about the technological details

- downloadable files are stored in a [content addressable storage/ cas](https://en.wikipedia.org/wiki/Content-addressable_storage)
- other data (configuration and content) is saved in a sqlite database using [Room](https://developer.android.com/topic/libraries/architecture/room); the "tables" are at the io.vertretungsplan.client.android.data.model package
- the UI uses [data binding](https://developer.android.com/topic/libraries/data-binding)
- background activities (syncing while the App is closed) are implemented using the [Work Manager](https://developer.android.com/topic/libraries/architecture/workmanager); the worker classes are at the io.vertretungsplan.client.android.worker package
- the logic for most actions is at the io.vertretungsplan.client.android.data.feature package
- the UI is in the io.vertretungsplan.client.android.ui package

### License

GPL 3.0

> vertretungsplan.io android client
>
> Copyright (C) 2019 - 2022 Jonas Lochmann
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, version 3 of the License.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see https://www.gnu.org/licenses/.
